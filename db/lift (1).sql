-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 09, 2018 at 01:11 PM
-- Server version: 5.7.22
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lift`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(50) NOT NULL,
  `code` varchar(7) NOT NULL,
  `role` int(1) NOT NULL COMMENT 'superadmin=1, admin_user=2',
  `image` varchar(100) NOT NULL,
  `status` int(1) NOT NULL COMMENT '0=active,1=inactive',
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `name`, `email`, `password`, `code`, `role`, `image`, `status`, `create_date`, `update_date`) VALUES
(1, 'Lift', 'admin@lift.com', 'fcea920f7412b5da7be0cf42b8c93759', 'cc3E4G', 1, 'd89d172402d91e367839fe07104afb1d_thumb.jpeg', 0, '2017-09-29 00:00:00', '2017-10-17 09:41:41');

-- --------------------------------------------------------

--
-- Table structure for table `contact_list`
--

CREATE TABLE `contact_list` (
  `contact_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `contact_user_id` int(10) NOT NULL,
  `contact_num` varchar(20) NOT NULL,
  `contact_name` varchar(50) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_list`
--

INSERT INTO `contact_list` (`contact_id`, `user_id`, `contact_user_id`, `contact_num`, `contact_name`, `create_date`, `update_date`) VALUES
(98, 14, 11, '9039003919', 'Anant', '2018-06-14 09:15:21', '2018-06-14 09:15:21'),
(99, 14, 16, '9845557199', 'Sumit Sahu', '2018-06-14 09:15:21', '2018-06-14 09:15:21'),
(132, 26, 12, '8109059067', 'Jaspal', '2018-06-25 03:52:49', '2018-06-25 03:52:49'),
(133, 26, 7, '8839649465', 'Sumit Jio', '2018-06-25 03:52:49', '2018-06-25 03:52:49'),
(134, 26, 1, '9754743271', 'Yatin', '2018-06-25 03:52:49', '2018-06-25 03:52:49'),
(145, 2, 10, '7771016011', 'Monti EB', '2018-06-30 06:17:43', '2018-06-30 06:17:43'),
(146, 2, 22, '9713031393', 'Uday Ebabu', '2018-06-30 06:17:43', '2018-06-30 06:17:43'),
(147, 2, 7, '8839649465', 'My Jio', '2018-06-30 06:17:43', '2018-06-30 06:17:43'),
(160, 33, 2, '8109059062', 'Ebabu Sumit', '2018-07-01 01:02:04', '2018-07-01 01:02:04'),
(161, 33, 22, '9713031393', 'Ebabu Uday', '2018-07-01 01:02:04', '2018-07-01 01:02:04'),
(162, 33, 24, '9713690098', 'Nishant Gupta', '2018-07-01 01:02:04', '2018-07-01 01:02:04'),
(163, 33, 14, '8966842340', 'Monika', '2018-07-01 01:02:04', '2018-07-01 01:02:04'),
(164, 33, 11, '9039003919', 'Self', '2018-07-01 01:02:04', '2018-07-01 01:02:04'),
(165, 33, 16, '9845557199', 'Sumit Mumbai', '2018-07-01 01:02:04', '2018-07-01 01:02:04'),
(172, 24, 14, '8966842340', 'Monika', '2018-07-01 02:24:45', '2018-07-01 02:24:45'),
(173, 24, 11, '9039003919', 'Anant 2', '2018-07-01 02:24:45', '2018-07-01 02:24:45'),
(174, 24, 33, '9229900141', 'Anant Mom.', '2018-07-01 02:24:45', '2018-07-01 02:24:45'),
(175, 24, 16, '9845557199', 'Sumit Bangalore', '2018-07-01 02:24:45', '2018-07-01 02:24:45'),
(185, 16, 24, '9713690098', 'Nishant Gupta', '2018-07-04 08:10:29', '2018-07-04 08:10:29'),
(186, 16, 11, '9039003919', 'Anant Shrivastava', '2018-07-04 08:10:29', '2018-07-04 08:10:29'),
(187, 16, 33, '9229900141', 'Anant Mom', '2018-07-04 08:10:29', '2018-07-04 08:10:29'),
(188, 11, 2, '8109059062', 'Ebabu Sumit', '2018-07-05 12:28:39', '2018-07-05 12:28:39'),
(189, 11, 22, '9713031393', 'Ebabu Uday', '2018-07-05 12:28:39', '2018-07-05 12:28:39'),
(190, 11, 24, '9713690098', 'Nishant Gupta', '2018-07-05 12:28:39', '2018-07-05 12:28:39'),
(191, 11, 14, '8966842340', 'Monika', '2018-07-05 12:28:39', '2018-07-05 12:28:39'),
(192, 11, 33, '9229900141', 'Mummy', '2018-07-05 12:28:39', '2018-07-05 12:28:39'),
(193, 11, 16, '9845557199', 'Sumit Mumbai', '2018-07-05 12:28:39', '2018-07-05 12:28:39'),
(194, 11, 34, '9893261990', 'Gupta Uncle Nishant', '2018-07-05 12:28:39', '2018-07-05 12:28:39'),
(200, 27, 10, '7771016011', 'Monti EB', '2018-07-07 06:56:05', '2018-07-07 06:56:05'),
(201, 27, 22, '9713031393', 'Uday Ebabu', '2018-07-07 06:56:05', '2018-07-07 06:56:05'),
(202, 27, 26, '7828437575', 'Harsh EB', '2018-07-07 06:56:05', '2018-07-07 06:56:05'),
(203, 27, 7, '8839649465', 'My Jio', '2018-07-07 06:56:05', '2018-07-07 06:56:05'),
(204, 27, 32, '9617822421', 'Jai EB', '2018-07-07 06:56:05', '2018-07-07 06:56:05');

-- --------------------------------------------------------

--
-- Table structure for table `email_template`
--

CREATE TABLE `email_template` (
  `template_id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `page_detail` text NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_template`
--

INSERT INTO `email_template` (`template_id`, `title`, `page_detail`, `create_date`) VALUES
(1, 'About us', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p>\\<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;</p>\n', '2018-03-29 00:00:00'),
(2, 'terms and condition', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p>\\<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;</p>\n', '2018-03-31 00:00:00'),
(3, 'Privacy Policy', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p>\\<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;</p>\n', '2018-03-31 00:00:00'),
(4, 'Return policy', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p>\\<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;</p>\n', '2018-03-31 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `emergency_contact`
--

CREATE TABLE `emergency_contact` (
  `contact_id` int(10) NOT NULL,
  `contact_name` varchar(100) NOT NULL,
  `contact_image` varchar(150) NOT NULL,
  `contact_num` varchar(20) NOT NULL,
  `user_id` int(10) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emergency_contact`
--

INSERT INTO `emergency_contact` (`contact_id`, `contact_name`, `contact_image`, `contact_num`, `user_id`, `create_date`, `update_date`) VALUES
(1, 'Sumit Yadav', '', '812059062', 1, '0000-00-00 00:00:00', '2018-03-28 11:42:54'),
(2, 'jaipal', '', '1111111122', 1, '2018-03-28 12:43:22', '2018-03-28 12:19:52'),
(17, 'Aakash kharcha', 'contact_image_7.jpeg', '8305620438', 7, '2018-03-31 12:07:50', '2018-03-31 11:08:29'),
(20, 'Mummy', '', '9229900141', 11, '2018-04-02 05:14:17', '2018-04-02 04:14:17'),
(21, 'Anant', '', '90390039', 14, '2018-06-14 10:18:55', '2018-06-14 09:18:55'),
(22, 'Sumit Mumbai', '', '9845557199', 11, '2018-06-14 10:46:27', '2018-06-14 09:46:27'),
(23, 'Monika', '', '8966842340', 11, '2018-06-14 10:58:51', '2018-06-14 09:58:51'),
(24, 'Anant 2', 'contact_image_1528971963411_24.jpg', '09039003919', 24, '2018-06-14 11:26:03', '2018-06-14 10:26:03'),
(25, 'Anshul Jio', '', '7987363445', 24, '2018-06-14 11:26:19', '2018-06-14 10:26:19'),
(26, 'Monika Jio', '', '87707879', 24, '2018-06-14 11:26:26', '2018-06-14 10:26:26'),
(27, 'MNJ1T', 'contact_image_1529590066176_2.jpg', '8109525129', 2, '2018-06-21 19:37:46', '2018-06-21 14:07:46'),
(28, 'Aashish', 'contact_image_1530106765310_2.jpg', '8982865218', 2, '2018-06-27 19:09:25', '2018-06-27 13:39:25'),
(29, 'Aashish Tooreest', '', '6585306334', 2, '2018-06-27 19:10:28', '2018-06-27 13:40:28');

-- --------------------------------------------------------

--
-- Table structure for table `end_ride_finder_detail`
--

CREATE TABLE `end_ride_finder_detail` (
  `id` int(10) NOT NULL,
  `finder_id` int(10) NOT NULL,
  `ride_id` int(10) NOT NULL,
  `offerer_end_ride_id` int(10) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `end_ride_finder_detail`
--

INSERT INTO `end_ride_finder_detail` (`id`, `finder_id`, `ride_id`, `offerer_end_ride_id`, `amount`, `create_date`) VALUES
(1, 27, 0, 6, '', '2018-07-09 13:31:18'),
(2, 38, 0, 7, '', '2018-07-09 16:26:41'),
(3, 22, 0, 1, '', '2018-07-09 17:07:12'),
(4, 27, 0, 1, '', '2018-07-09 17:07:12'),
(5, 27, 0, 1, '', '2018-07-09 17:43:24'),
(6, 22, 0, 1, '', '2018-07-09 17:43:24'),
(7, 39, 0, 1, '', '2018-07-09 17:43:24'),
(8, 27, 0, 2, '', '2018-07-09 18:05:51'),
(9, 39, 0, 2, '', '2018-07-09 18:05:51'),
(10, 22, 0, 2, '', '2018-07-09 18:05:51'),
(11, 37, 0, 3, '', '2018-07-09 18:11:43'),
(12, 37, 0, 4, '', '2018-07-09 18:37:53');

-- --------------------------------------------------------

--
-- Table structure for table `facebook_friend_list`
--

CREATE TABLE `facebook_friend_list` (
  `friend_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `friend_user_id` int(10) NOT NULL,
  `facebook_id` varchar(50) NOT NULL,
  `facebook_frnd_name` varchar(100) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facebook_friend_list`
--

INSERT INTO `facebook_friend_list` (`friend_id`, `user_id`, `friend_user_id`, `facebook_id`, `facebook_frnd_name`, `create_date`, `update_date`) VALUES
(2, 1, 23, '1111111111111', 'Yash', '2018-04-30 07:25:01', '2018-04-30 07:25:01'),
(3, 1, 7, '1899423096748599', 'sumit yadav', '2018-04-30 07:25:01', '2018-04-30 07:25:01');

-- --------------------------------------------------------

--
-- Table structure for table `find_ride`
--

CREATE TABLE `find_ride` (
  `find_id` int(10) NOT NULL,
  `finder_id` int(10) NOT NULL,
  `source_location` text NOT NULL,
  `destination_location` text NOT NULL,
  `source_lat` varchar(20) NOT NULL,
  `source_lng` varchar(20) NOT NULL,
  `destination_lat` varchar(20) NOT NULL,
  `destination_lng` varchar(20) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `vehicle_type` int(1) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL,
  `actual_time` datetime NOT NULL,
  `no_of_seats` bit(1) NOT NULL DEFAULT b'1',
  `ride_status_complete` int(1) NOT NULL COMMENT '0=incomp,1=comple',
  `lift_id` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `find_ride`
--

INSERT INTO `find_ride` (`find_id`, `finder_id`, `source_location`, `destination_location`, `source_lat`, `source_lng`, `destination_lat`, `destination_lng`, `start_time`, `end_time`, `vehicle_type`, `create_date`, `update_date`, `status`, `actual_time`, `no_of_seats`, `ride_status_complete`, `lift_id`) VALUES
(1, 39, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Khajrana Main Rd, Goyal Vihar Avenue, Indore, Madhya Pradesh 452016, India', '22.722647162190512', '75.88721361011268', '22.731763970602', '75.904280841351', '2018-07-09 17:28:00', '2018-07-09 17:48:00', 2, '2018-07-09 17:38:38', '2018-07-09 12:10:04', 2, '2018-07-09 17:38:00', b'1', 0, ''),
(2, 22, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Khajrana Main Rd, Goyal Vihar Avenue, Indore, Madhya Pradesh 452016, India', '22.722643451144485', '75.88720925152302', '22.731763970602', '75.904280841351', '2018-07-09 17:28:00', '2018-07-09 17:48:00', 2, '2018-07-09 17:38:39', '2018-07-09 17:44:01', 4, '2018-07-09 17:38:00', b'1', 4, ''),
(3, 27, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Khajrana Main Rd, Goyal Vihar Avenue, Indore, Madhya Pradesh 452016, India', '22.722643760398327', '75.88720723986626', '22.731763970602', '75.904280841351', '2018-07-09 17:28:00', '2018-07-09 17:48:00', 2, '2018-07-09 17:38:40', '2018-07-09 17:52:18', 4, '2018-07-09 17:38:00', b'1', 4, ''),
(4, 39, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Khajrana Main Rd, Goyal Vihar Avenue, Indore, Madhya Pradesh 452016, India', '22.722647162190512', '75.88721361011268', '22.731763970602', '75.904280841351', '2018-07-09 17:30:00', '2018-07-09 17:50:00', 2, '2018-07-09 17:40:19', '2018-07-09 17:49:28', 4, '2018-07-09 17:40:00', b'1', 4, ''),
(7, 39, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Khajrana Main Rd, Goyal Vihar Avenue, Indore, Madhya Pradesh 452016, India', '22.7226428326368', '75.88720858097076', '22.731763970602', '75.904280841351', '2018-07-09 17:47:00', '2018-07-09 18:07:00', 2, '2018-07-09 17:57:38', '2018-07-09 12:28:17', 2, '2018-07-09 17:57:00', b'1', 0, ''),
(8, 22, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Khajrana Main Rd, Goyal Vihar Avenue, Indore, Madhya Pradesh 452016, India', '22.722644069652162', '75.88720723986626', '22.731763970602', '75.904280841351', '2018-07-09 17:47:00', '2018-07-09 18:07:00', 2, '2018-07-09 17:57:40', '2018-07-09 12:28:19', 2, '2018-07-09 17:57:00', b'1', 0, ''),
(9, 39, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Khajrana Main Rd, Goyal Vihar Avenue, Indore, Madhya Pradesh 452016, India', '22.7226428326368', '75.88720858097076', '22.731763970602', '75.904280841351', '2018-07-09 17:48:00', '2018-07-09 18:08:00', 2, '2018-07-09 17:58:28', '2018-07-09 13:10:25', 1, '2018-07-09 17:58:00', b'1', 1, ''),
(10, 22, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Khajrana Main Rd, Goyal Vihar Avenue, Indore, Madhya Pradesh 452016, India', '22.722644069652162', '75.88720723986626', '22.731763970602', '75.904280841351', '2018-07-09 17:48:00', '2018-07-09 18:08:00', 2, '2018-07-09 17:58:29', '2018-07-09 13:05:51', 1, '2018-07-09 17:58:00', b'1', 1, ''),
(11, 27, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Khajrana Main Rd, Goyal Vihar Avenue, Indore, Madhya Pradesh 452016, India', '22.722643451144485', '75.88720824569464', '22.731763970602', '75.904280841351', '2018-07-09 17:50:00', '2018-07-09 18:10:00', 2, '2018-07-09 18:00:51', '2018-07-09 12:54:36', 4, '2018-07-09 18:00:00', b'1', 1, ''),
(12, 37, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Vijay Nagar Square, Vijay Nagar, Indore, Madhya Pradesh, India', '22.722636029052115', '75.88720925152302', '22.751281069071', '75.895158313215', '2018-07-09 18:00:00', '2018-07-09 18:20:00', 1, '2018-07-09 18:10:52', '2018-07-09 18:35:13', 3, '2018-07-09 18:10:00', b'1', 1, ''),
(13, 22, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Khajrana Main Rd, Goyal Vihar Avenue, Indore, Madhya Pradesh 452016, India', '22.722674067271225', '75.88716264814138', '22.731763970602', '75.904280841351', '2018-07-09 18:14:00', '2018-07-09 18:34:00', 1, '2018-07-09 18:24:21', '2018-07-09 12:54:26', 2, '2018-07-09 18:24:00', b'1', 0, ''),
(14, 37, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Vijay Nagar Square, Vijay Nagar, Indore, Madhya Pradesh, India', '22.722636029052115', '75.88720925152302', '22.751281069071', '75.895158313215', '2018-07-09 18:20:00', '2018-07-09 18:40:00', 1, '2018-07-09 18:30:22', '2018-07-09 13:04:51', 2, '2018-07-09 18:30:00', b'1', 0, ''),
(15, 37, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Vijay Nagar Square, Vijay Nagar, Indore, Madhya Pradesh, India', '22.722636029052115', '75.88720925152302', '22.751281069071', '75.895158313215', '2018-07-09 18:34:00', '2018-07-09 18:54:00', 1, '2018-07-09 18:34:57', '2018-07-09 13:05:11', 2, '2018-07-09 18:44:00', b'1', 0, ''),
(16, 37, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Vijay Nagar Square, Vijay Nagar, Indore, Madhya Pradesh, India', '22.722636029052115', '75.88720925152302', '22.751281069071', '75.895158313215', '2018-07-09 18:26:00', '2018-07-09 18:46:00', 1, '2018-07-09 18:36:47', '2018-07-09 13:07:38', 1, '2018-07-09 18:36:00', b'1', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `friend_list`
--

CREATE TABLE `friend_list` (
  `friend_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `user_friend_id` int(10) NOT NULL,
  `request_status` int(1) NOT NULL COMMENT '0=pending,1=accept,2=reject',
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `friend_list`
--

INSERT INTO `friend_list` (`friend_id`, `user_id`, `user_friend_id`, `request_status`, `create_date`) VALUES
(6, 27, 2, 1, '2018-06-27 11:11:01'),
(8, 2, 16, 1, '2018-07-04 20:11:07'),
(11, 2, 26, 0, '2018-07-03 20:51:17'),
(12, 38, 22, 1, '2018-07-09 15:48:28');

-- --------------------------------------------------------

--
-- Table structure for table `offer_ride`
--

CREATE TABLE `offer_ride` (
  `offer_id` int(10) NOT NULL,
  `offerer_id` int(10) NOT NULL,
  `source_location` text NOT NULL,
  `destination_location` text NOT NULL,
  `source_lat` varchar(20) NOT NULL,
  `source_lng` varchar(20) NOT NULL,
  `destination_lat` varchar(20) NOT NULL,
  `destination_lng` varchar(20) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `vehicle_type` int(1) NOT NULL COMMENT '1=bike , 2= car',
  `vehicle_id` int(1) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL COMMENT '1=active',
  `no_of_seats` int(2) NOT NULL,
  `actual_time` datetime NOT NULL,
  `estimated_cost` varchar(30) NOT NULL,
  `ride_status_complete` int(1) NOT NULL COMMENT '0=incomp,1=comple',
  `lift_id` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offer_ride`
--

INSERT INTO `offer_ride` (`offer_id`, `offerer_id`, `source_location`, `destination_location`, `source_lat`, `source_lng`, `destination_lat`, `destination_lng`, `start_time`, `end_time`, `vehicle_type`, `vehicle_id`, `create_date`, `update_date`, `status`, `no_of_seats`, `actual_time`, `estimated_cost`, `ride_status_complete`, `lift_id`) VALUES
(1, 2, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', '52, Khajrana Main Rd, Goyal Vihar Colony, Ganeshpuri, Khajrana, Indore, Madhya Pradesh 452016, India', '22.7226428326368', '75.88720690459013', '22.731763042902', '75.904280841351', '2018-07-09 17:28:00', '2018-07-09 17:48:00', 2, 38, '2018-07-09 17:38:37', '2018-07-09 12:30:37', 4, 3, '2018-07-09 17:38:00', '11', 1, ''),
(2, 2, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', '52, Khajrana Main Rd, Goyal Vihar Colony, Ganeshpuri, Khajrana, Indore, Madhya Pradesh 452016, India', '22.72264252338296', '75.88720791041851', '22.731763042902', '75.904280841351', '2018-07-09 17:50:00', '2018-07-09 18:10:00', 2, 38, '2018-07-09 18:00:47', '2018-07-09 12:34:13', 1, 3, '2018-07-09 18:00:00', '11', 1, ''),
(3, 35, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'A/2, 902, Vijay Nagar Square, Scheme Number 136, Indore, Madhya Pradesh 452010, India', '22.722646234429018', '75.88721361011268', '22.751287562042', '75.895158313215', '2018-07-09 18:00:00', '2018-07-09 18:20:00', 1, 43, '2018-07-09 18:10:51', '2018-07-09 12:41:02', 1, 1, '2018-07-09 18:10:00', '18', 1, ''),
(4, 37, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'A/2, 902, Vijay Nagar Square, Scheme Number 136, Indore, Madhya Pradesh 452010, India', '22.722636029052115', '75.88720925152302', '22.751281069071', '75.895158313215', '2018-07-09 18:15:00', '2018-07-09 18:35:00', 0, 0, '2018-07-09 18:25:45', '2018-07-09 12:59:59', 2, 1, '2018-07-09 18:25:00', '18', 0, ''),
(5, 39, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'A/2, 902, Vijay Nagar Square, Scheme Number 136, Indore, Madhya Pradesh 452010, India', '22.722643451144485', '75.88720925152302', '22.751288489609', '75.895158313215', '2018-07-09 18:24:00', '2018-07-09 18:44:00', 0, 0, '2018-07-09 18:34:27', '2018-07-09 13:04:53', 2, 1, '2018-07-09 18:34:00', '18', 0, ''),
(6, 39, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'A/2, 902, Vijay Nagar Square, Scheme Number 136, Indore, Madhya Pradesh 452010, India', '22.722643451144485', '75.88720925152302', '22.751288489609', '75.895158313215', '2018-07-09 18:25:00', '2018-07-09 18:45:00', 0, 0, '2018-07-09 18:35:00', '2018-07-09 13:05:13', 2, 1, '2018-07-09 18:35:00', '18', 0, ''),
(7, 39, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Gemini Mall, Ware House Rd, LIG Colony, Indore, Madhya Pradesh 452011, India', '22.722643451144485', '75.88720925152302', '22.746264075101', '75.892463028431', '2018-07-09 18:26:00', '2018-07-09 18:46:00', 1, 45, '2018-07-09 18:36:42', '2018-07-09 13:06:42', 1, 1, '2018-07-09 18:36:00', '15', 0, ''),
(8, 27, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'A/2, 902, Vijay Nagar Square, Scheme Number 136, Indore, Madhya Pradesh 452010, India', '22.722644069652162', '75.88720757514238', '22.751288489609', '75.895158313215', '2018-07-09 18:27:00', '2018-07-09 18:47:00', 1, 39, '2018-07-09 18:37:35', '2018-07-09 13:07:38', 1, 1, '2018-07-09 18:37:00', '18', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `rating_review`
--

CREATE TABLE `rating_review` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `ride_id` int(10) NOT NULL,
  `offer_id` int(10) NOT NULL,
  `second_user_id` int(10) NOT NULL,
  `rating` int(1) NOT NULL,
  `review` varchar(200) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rating_by` int(1) NOT NULL COMMENT '1=offer,2=finder'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ride_detail`
--

CREATE TABLE `ride_detail` (
  `id` int(10) NOT NULL,
  `offerer_id` int(10) NOT NULL,
  `offer_id` int(10) NOT NULL,
  `lat` varchar(30) NOT NULL,
  `lng` varchar(30) NOT NULL,
  `source_location` varchar(300) NOT NULL,
  `destination_location` varchar(300) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `match_id` varchar(30) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `total_seats` int(1) NOT NULL,
  `seats_left_complete` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_detail`
--

INSERT INTO `ride_detail` (`id`, `offerer_id`, `offer_id`, `lat`, `lng`, `source_location`, `destination_location`, `create_date`, `update_date`, `match_id`, `status`, `total_seats`, `seats_left_complete`) VALUES
(1, 2, 1, '', '', '', '', '2018-07-09 17:43:24', '2018-07-09 17:52:18', '1,2,3', 4, 3, 0),
(2, 2, 2, '22.7226362', '75.8872074', '', '', '2018-07-09 18:05:51', '2018-07-09 18:37:53', '4,5,6', 1, 3, 2),
(3, 35, 3, '', '', '', '', '2018-07-09 18:11:43', '2018-07-09 12:41:43', '7', 1, 1, 1),
(4, 27, 8, '', '', '', '', '2018-07-09 18:37:53', '2018-07-09 13:07:53', '8', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ride_matches`
--

CREATE TABLE `ride_matches` (
  `match_id` int(10) NOT NULL,
  `ride_id` int(10) NOT NULL,
  `second_ride_id` int(10) NOT NULL,
  `offerer_id` int(10) NOT NULL,
  `finder_id` int(10) NOT NULL,
  `ride_type` int(1) NOT NULL COMMENT '1=offer, 2=find',
  `ride_status` int(1) NOT NULL,
  `ride_datetime` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `otp_code` varchar(4) NOT NULL,
  `cancelled_by` int(1) NOT NULL,
  `amount` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_matches`
--

INSERT INTO `ride_matches` (`match_id`, `ride_id`, `second_ride_id`, `offerer_id`, `finder_id`, `ride_type`, `ride_status`, `ride_datetime`, `update_date`, `otp_code`, `cancelled_by`, `amount`) VALUES
(1, 3, 1, 2, 27, 2, 8, '2018-07-09 17:39:11', '2018-07-09 17:52:18', '', 0, '6'),
(2, 2, 1, 2, 22, 2, 8, '2018-07-09 17:39:13', '2018-07-09 12:16:11', '', 0, '6'),
(3, 4, 1, 2, 39, 2, 8, '2018-07-09 17:40:26', '2018-07-09 17:49:28', '', 0, '6'),
(4, 11, 2, 2, 27, 2, 8, '2018-07-09 18:00:56', '2018-07-09 13:10:31', '', 0, '6'),
(5, 2, 9, 2, 39, 1, 2, '2018-07-09 18:00:57', '2018-07-09 13:10:31', '', 0, '6'),
(6, 10, 2, 2, 22, 2, 2, '2018-07-09 18:01:06', '2018-07-09 13:10:31', '', 0, '6'),
(7, 12, 3, 35, 37, 2, 2, '2018-07-09 18:10:58', '2018-07-09 12:41:43', '', 0, ''),
(8, 16, 8, 27, 37, 2, 2, '2018-07-09 18:37:38', '2018-07-09 13:07:53', '', 0, '89');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `autorejectiontime` varchar(10) NOT NULL,
  `minridetime` varchar(10) NOT NULL,
  `maxridetime` varchar(10) NOT NULL,
  `sourcedistance` varchar(10) NOT NULL,
  `bikeperkm` varchar(10) NOT NULL,
  `carperkm` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `autorejectiontime`, `minridetime`, `maxridetime`, `sourcedistance`, `bikeperkm`, `carperkm`) VALUES
(1, '', '5', '10', '', '', ''),
(2, '10', '', '', '', '', ''),
(3, '', '', '', '100', '', ''),
(4, '', '', '', '', '50', '10');

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE TABLE `support` (
  `support_id` int(8) NOT NULL,
  `user_id` int(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `support`
--

INSERT INTO `support` (`support_id`, `user_id`, `title`, `description`, `create_date`) VALUES
(1, 1, 'need help', 'How to find driver', '2018-03-29 07:49:05'),
(2, 7, 'Suggestion', 'Testing support message to test support functionality.', '2018-04-04 11:03:09'),
(3, 11, 'Compliment', 'There are many things which are pending..a lot of corrections is to be done', '2018-06-14 11:01:33'),
(4, 24, 'General', 'Ghhejk jehjskk jsjhdlk kshhakkd ijshhakkd kjsghakdb jshh', '2018-06-14 11:21:27'),
(5, 24, 'Suggestion', 'Hhsiid jshhskkf jjsghakkf judggsjks jdjjsbbtk jsjhsj', '2018-06-14 11:22:24'),
(6, 24, 'Compliment', 'Hhsggaj bdghdk jsvgdkd kjsvbsk jsbhdkkdv jshhskks jjdh', '2018-06-14 11:23:18'),
(7, 24, 'Question', 'Hhdjk hshjdk jsghskkf jjsgyskk jshhskks jjdgjskbjsyhsk', '2018-06-14 11:23:43'),
(8, 24, 'General', 'Hhff hjkk hffv jjgg jjjfd jkhfd kkgff kkg jjff khfbkv k kigff kjfv', '2018-07-01 02:36:01');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(10) NOT NULL,
  `facebook_id` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_facebook_id` varchar(100) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_mobile` varchar(20) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_refer_code` varchar(10) NOT NULL COMMENT 'self',
  `user_referal_code` varchar(10) NOT NULL COMMENT 'other user',
  `user_gender` int(1) NOT NULL COMMENT '1=male,2=female',
  `user_image` varchar(200) NOT NULL,
  `user_mobile_status` int(1) NOT NULL COMMENT '1=active',
  `mobile_code` varchar(4) NOT NULL,
  `admin_status` int(1) NOT NULL COMMENT '1=active',
  `user_token` varchar(100) NOT NULL,
  `registered_device_id` varchar(100) NOT NULL,
  `user_device_type` varchar(10) NOT NULL,
  `user_device_id` varchar(100) NOT NULL,
  `user_device_token` text NOT NULL,
  `user_wallet` varchar(15) NOT NULL,
  `user_lat` varchar(25) NOT NULL,
  `user_lng` varchar(15) NOT NULL,
  `user_home_location` varchar(255) NOT NULL,
  `user_work_location` varchar(255) NOT NULL,
  `user_other_location` varchar(255) NOT NULL,
  `user_home_lat` varchar(25) NOT NULL,
  `user_home_lng` varchar(25) NOT NULL,
  `user_work_lat` varchar(25) NOT NULL,
  `user_work_lng` varchar(25) NOT NULL,
  `user_other_lat` varchar(25) NOT NULL,
  `user_other_lng` varchar(25) NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `corporate_email` varchar(200) NOT NULL,
  `emergancy_contact` varchar(20) NOT NULL,
  `profession` varchar(100) NOT NULL,
  `about` varchar(200) NOT NULL,
  `user_license_num` varchar(30) NOT NULL,
  `license_image` varchar(150) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Lift_id` varchar(10) NOT NULL,
  `total_offer` int(5) NOT NULL,
  `total_find` int(5) NOT NULL,
  `avg_rating` decimal(5,2) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `find_id` int(11) NOT NULL,
  `current_ride_id` int(11) NOT NULL,
  `current_status` int(1) NOT NULL COMMENT '1=offer_ride, 2=find_ride, 3=offerbyfriend, 4=findtofrnd, 5=complete, 6=cancel, 7=start_ride'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `facebook_id`, `user_name`, `user_facebook_id`, `user_email`, `user_mobile`, `user_password`, `user_refer_code`, `user_referal_code`, `user_gender`, `user_image`, `user_mobile_status`, `mobile_code`, `admin_status`, `user_token`, `registered_device_id`, `user_device_type`, `user_device_id`, `user_device_token`, `user_wallet`, `user_lat`, `user_lng`, `user_home_location`, `user_work_location`, `user_other_location`, `user_home_lat`, `user_home_lng`, `user_work_lat`, `user_work_lng`, `user_other_lat`, `user_other_lng`, `company_name`, `corporate_email`, `emergancy_contact`, `profession`, `about`, `user_license_num`, `license_image`, `create_date`, `update_date`, `Lift_id`, `total_offer`, `total_find`, `avg_rating`, `offer_id`, `find_id`, `current_ride_id`, `current_status`) VALUES
(2, '1899423096748599', 'Sumit Yadav ', '', '', '8109059062', '123456', '8NEPJC', 'R26CK5', 1, 'user_image_1530616402354_2.jpeg', 1, '1234', 1, 'd96847f2ece57b6386bedf4c24e82f684ab05eca1531112986423', '1234567890aaaaaaaa', 'android', 'ZY223HXKTT', 'eah7AVUhYts:APA91bHK-yl02lNjYojkyHi4Fmwuo6hk02FYzf06n78Za3ICrftx320MSHhI-InEW_csOrTgUVT83fKML3giKiPQrQTFxDAReQKSlhN3bCTL41D6EimPpFnWkGu_cUYo1GKCeX_IucJQMEs9IQ4nevHNrFG6i5JfRw', '3179', '', '', '', '', '', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', '2018-03-19 08:30:27', '2018-07-09 12:49:36', 'LIFT02', 15, 12, '5.00', 0, 0, 2, 0),
(3, '1111111111111', '', '', 'yatindra.mohite@ebabu.co', '', '', '', '', 0, '', 1, '0', 1, '0b1d1ecf9673441f75bf1b9bd038b6c7637e9754152', '1234567890aaaaaa11', 'android', '1234567890aaaaaa11', 'f4s6s66f4f64sf6s46s4f654fs6f4s6fs6f46fs6f4s6s66f', '20', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-06-22 00:00:00', '2018-06-26 07:44:45', 'LIFT03', 0, 0, '0.00', 0, 0, 0, 0),
(4, '', 'Sumit', '', '', '8109059063', 'abc123', 'SLEQRS', '', 0, '', 1, '1234', 1, '', '', 'android', 'ZY223HXKTT', '', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-23 01:55:31', '2018-07-04 13:10:39', 'LIFT04', 0, 0, '0.00', 0, 0, 0, 0),
(5, '', 'Abc', '', '', '9123456789', 'abc123', 'LQOFOW', '', 0, '', 0, '1234', 1, '', '', 'android', 'ZY223HXKTT', '', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-23 02:21:08', '2018-07-04 13:10:39', 'LIFT05', 0, 0, '0.00', 0, 0, 0, 0),
(7, '', 'Sumit', '', 'sumity90@gmail.com', '8839649465', 'abc123', 'SUMIT1', '', 1, 'user_image_1522757267390_7.jpeg', 1, '0', 1, '', 'ZY223HXKTT', 'android', 'ZY223HXKTT', '', '', '', '', 'Chhoti Gwaltoli Main Rd, Siyaganj, Indore, Madhya Pradesh 452007, India', 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', '56 Dukan St, Chappan Dukan, New Palasia, Indore, Madhya Pradesh 452001, India', '22.715963799999994', '75.8649566', '22.722727', '75.88707099999999', '22.724082100000004', '75.8846182', 'Engineerbabu', 'sumit.yadav@ebabu.co', '', 'Engineer', 'Android developer at engineerbabu', 'Mp12345678', 'user_lic_image_1522915325723_7.jpeg', '2018-03-27 08:42:40', '2018-07-09 07:47:02', 'LIFT07', 4, 5, '0.00', 0, 0, 0, 0),
(8, '102751848487306351855', '', '', 'sumity90@gmail.com', '', '', '', '', 0, '', 0, '0', 1, '', 'ZY223HXKTT', 'android', 'ZY223HXKTT', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-27 11:49:55', '2018-07-04 13:10:39', 'LIFT08', 0, 0, '0.00', 0, 0, 0, 0),
(9, '102783611205631119366', '', '', 'montiparauha812@gmail.com', '', '', 'H7ZZLN', '', 0, '', 0, '0', 1, '', '', 'android', '268cdc1c', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-31 03:09:43', '2018-07-04 13:10:39', 'LIFT09', 0, 0, '0.00', 0, 0, 0, 0),
(10, '', 'Monti', '', '', '7771016011', '123456', 'J8DOOB', '', 1, '', 1, '0', 1, 'cb2ef1a26cdec7532b5810e1af68e0c70c3e89061522931519986', '268cdc1c', 'android', '268cdc1c', 'cQPiD_OvTVs:APA91bH3b_HpGKV0HoaT1_cbJ-nJ55WZWrQZxBW090R2ct0EDrH1_fpETP0tQGsavAqlTsx0i7tbEZvInXCMwx2wCJuzUIddX9FPv9jIojG_MVkbDyrx3IDuwuCNoeYNEaQEzOt9Oo43', '20', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-31 03:10:31', '2018-07-04 13:15:11', 'LIFT10', 0, 0, '0.00', 0, 0, 0, 0),
(11, '10209046604402247', 'Anant Shrivastava', '', 'anant_abstruse@yahoo.co.in', '9039003919', 'liftowner2', 'RBQLOF', '', 1, 'https://graph.facebook.com/10209046604402247/picture?type=large', 1, '1234', 1, '62b1f8dec124bc42dd4a015ae2ad183b006909261530790565199', 'GBAXGY11A389ASN', 'android', 'GBAXGY11A389ASN', 'c9LC5nXCqZs:APA91bEvHdfjX5XjKfl019JkZeKMuCEmMQrldGFaJGLax2Jbktrq_fHpFacg6EXatYzSwLVpIHrqQebnDXy9hfiTqLLWIhJCpblal5wCZ4dmfaExKberBWM_o8MkmPqf7ff4zZz8xJoi', '60', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-31 03:23:15', '2018-07-05 11:36:05', 'LIFT11', 2, 3, '0.00', 0, 0, 0, 0),
(12, '', 'Sumit', '', '', '8109059067', 'abc123', 'VZYUDF', '', 1, '', 1, '0', 1, '', 'ZY223HXKTT', 'android', 'ZY223HXKTT', '', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-31 04:18:00', '2018-07-04 13:15:11', 'LIFT12', 0, 0, '0.00', 0, 0, 0, 0),
(13, '', 'Bilal', '', '', '9606887838', '12345678', 'QXTDNN', '', 1, '', 1, '0', 1, '125cb0194fc782d7448459d13ad452294b4e94b71522517093894', 'ZY222WT9GF', 'android', 'ZY222WT9GF', 'd9dPD_LqsR0:APA91bEvaPe0hYEXm9rJo8SUsM51I-xPlR-ZeI-sC-oSJ6EVyX89TfDylhH4AYgsL3N1oOB8nrG2W9AUJRdLbtvJRT1bkKKTTwcuWVXSaDPYie-I9BMXebUIew38nSU1h1-Az7yB60ZQ', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-31 06:24:50', '2018-07-04 13:15:11', 'LIFT13', 0, 0, '0.00', 0, 0, 0, 0),
(14, '', 'Monika Hassija', '', '', '8966842340', 'moni1990has', 'WYWWJD', '', 1, '', 1, '0', 1, 'b57b68988a2cdc5be135abe4ba0e43bf34786c621530778741274', '493b2663', 'android', '493b2663', 'ePSXSHdk0JU:APA91bGQ9NmkoATDM4PRNdRc2h8jjT9jXrPL5gG3_EqVHJqkytODEVz2bgQEfgDPXClJ1SAP0A5siD1bm2AwbTuqmt1XMpPi0wW1Qfyz7h30FRcAssu-Xq_oTsz0DfyOIbpWUBYLFemh', '10', '', '', '', '', '', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', '2018-04-02 05:41:39', '2018-07-05 08:19:01', 'LIFT14', 4, 0, '0.00', 0, 0, 0, 0),
(15, '113552304616879986908', '', '', 'sumit.yadav@ebabu.co', '', '', 'U8VWTL', '', 0, '', 0, '0', 1, '', '', 'android', 'ZY223HXKTT', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-04-10 10:40:40', '2018-07-08 20:38:26', 'LIFT15', 0, 0, '0.00', 0, 0, 0, 0),
(16, '2065322276842276', 'Sumit', '', 'sumitsahu123@gmail.com', '9845557199', 'guddodi15', '61IAHU', '', 1, '', 1, '0', 1, 'e7540f8667010df9f5b44b98f45b470c12e714201527435317749', '05fbdb400ae56c29', 'android', '05fbdb400ae56c29', 'dhK0PivY3iY:APA91bGwYKGr97PGxdmHsAF_WC9dpCHzuO6dk9Qo2GdvgbzMA-zET53XGKPHX17BmI-GUChGPq9G6TJ_BJ4KcZoyg51MPd_FJw2C_KFHeCcwFNiKP_b-83fqqinrZPqb33S6oFbKaEgk', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-04-10 06:35:05', '2018-07-04 14:45:43', 'LIFT16', 0, 1, '0.00', 0, 0, 0, 0),
(17, '137142860462149', '', '', 'remo_egijanr_remo@tfbnw.net', '', '', 'W3J6HA', '', 0, '', 0, '0', 1, '', '', 'android', '54358ec6', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-04-11 11:16:10', '2018-07-04 13:15:11', 'LIFT17', 0, 0, '0.00', 0, 0, 0, 0),
(18, '107819113407654', '', '', 'jaipal_rjtconz_solanki@tfbnw.net', '', '', 'GNM5YX', '', 0, '', 0, '0', 1, '', '', 'android', '54358ec6', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-04-11 11:35:01', '2018-07-04 13:15:11', 'LIFT18', 0, 0, '0.00', 0, 0, 0, 0),
(19, '110581479797258', '', '', 'monika_dnczhrz_gupta@tfbnw.net', '', '', 'FFH4TN', '', 0, '', 0, '0', 1, '', '', 'android', '54358ec6', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-04-11 11:36:19', '2018-07-04 13:15:11', 'LIFT19', 0, 0, '0.00', 0, 0, 0, 0),
(20, '122229388629953', '', '', 'harsh_izwgvas_parashar@tfbnw.net', '', '', 'BCDPA7', '', 0, '', 0, '0', 1, '', '', 'android', '54358ec6', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-04-11 11:37:26', '2018-07-04 13:15:11', 'LIFT20', 0, 0, '0.00', 0, 0, 0, 0),
(21, '116497657947397447802', 'Ghf', '', 'paytmrisk.assessment@gmail.com', '8800548368', 'tyagi123', 'UYMWGY', '', 1, '', 0, '1234', 1, '', '', 'android', 'ZW2223XTNG', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-05-25 12:00:21', '2018-07-04 13:15:11', 'LIFT21', 0, 0, '0.00', 0, 0, 0, 0),
(22, '', 'Uday', '', '', '9713031393', 'abc123', '7LNEIV', '', 1, '', 1, '0', 1, 'ab18f444b83b4575bfcf529797b1cbbf71db34201531122630910', '54358ec6', 'android', '42002c66d88d44c5', 'eE9_pnM4eGc:APA91bHFUwx227f9qh1wQZYT1RjsBnJbaMsHXK7YjwIBsTS6IiIK3HCr0fZLz0jT_v6sWQx-TVU20WgtEtWZjJvBUNpo1VFgWPgKRGfzVZCWCYvAx98ahUB0Wsv2rMzLt_IXePmcznu0ngRU0pHRQKnPKluQPNtBvA', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-05-29 11:40:26', '2018-07-09 12:54:26', 'LIFT22', 10, 15, '4.00', 0, 13, 2, 6),
(23, '', '', '', 'sumity90@gmail.com', '', '', 'LVSCO6', '', 0, '', 0, '0', 1, '', '', 'android', 'ZY223HXKTT', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-06-01 02:51:27', '2018-07-04 13:15:11', 'LIFT23', 0, 0, '0.00', 0, 0, 0, 0),
(24, '1743811715666693', 'Nishant', '', '', '9713690098', '123456', 'MWIVU9', 'RBQLOF', 1, 'user_image_1528972924607_24.jpeg', 1, '1234', 1, '2fd4f4673b136582d92b0f5008baf519de9f295f1530443229273', 'QS7HQWROAIKVJFEE', 'android', 'QS7HQWROAIKVJFEE', 'fCoNtuARP-0:APA91bGjaGfOjTkB8P2jD-WwSIOXjCXN7h-HfI_9ZNcX93x389y0lOdzAgJnXj0j-c0LoSXIzoKHmoV9yOJUtp4yqWqk9Le4Enp-jcftLhY-IY-xclr2fAj4kiE6R6EcbS4FkdDTCHL7TdP1BkIrHXs5iZfUA4ZcsQ', '10', '', '', 'Bhopal Rd, Shala, Madhya Pradesh 466331, India', 'House no 133, 5th Cross, Ranganath colony, Opp BHEL., Mysore Rd, Ranganathan Colony, Deepanjali Nagar, Bengaluru, Karnataka 560026, India', 'Hinjewadi Ph-2 Rd, Phase 2, Hinjewadi Rajiv Gandhi Infotech Park, Hinjawadi, Pimpri-Chinchwad, Maharashtra 411057, India', '22.690516199999998', '77.2947057', '12.9465294', '77.53587089999999', '18.594330400000004', '73.7095995', 'Hdjdjj', 'gehshdb@gshsh.com', '', 'Jdjdjj', 'Hdhdbje hshwh jdjei jwiwo jdjd isuej jdowkkfj', 'Dghd52618gsh6262727', 'user_lic_image_1528972924607_24.jpeg', '2018-06-14 10:44:31', '2018-07-04 13:15:11', 'LIFT24', 2, 4, '0.00', 0, 0, 0, 0),
(25, '11111111111122', '', '', 'yatindra.mohite43@ebabu.co', '', '', 'CY51IX', '', 0, '', 0, '0', 1, '', '', 'android', '1234567890aaaaaa1122', 'f4s6s66f4f64sf6s46s4f654fs6f4s6fs6f46fs6f4s6s66f', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-06-21 07:23:02', '2018-07-04 13:15:11', 'LIFT25', 0, 0, '0.00', 0, 0, 0, 0),
(26, '', 'HarshWardhan Prashar', '', '', '7828437575', 'abc123', 'QILZES', 'LIFT02', 1, '', 1, '0', 1, '', 'ZY223HXKTT', 'android', 'ZY223HXKTT', '', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-06-25 01:31:48', '2018-07-09 07:47:29', 'LIFT26', 0, 0, '0.00', 0, 0, 0, 0),
(27, '105104809250139918688', 'EB Test', '', 'ebtestdevice1@gmail.com', '9876543210', 'abc123', '', '', 2, '', 1, '', 1, '32470e4c969c49eab5295010173fcb2a500b0bf21531116628062', 'ZY223HXKTT', 'android', '4200aac3dab8745f', 'dN0slqbiAVw:APA91bHdeDxh5YeFSx8o4sxo-a1_ieJtrQ_pcdB7eZ2-dhFRdrQu-zd7ybOrboL6_6ypqCaUSh3FilwaCqyHcU2QPLZHckoRi46B8_g_cHwtZTIYg2gBPLv3_mrg90etF208sza1EcvtOMq_7TEk9HxoO5H9Jx6KKg', '10', '', '', '', '', '', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', '2018-06-26 01:10:01', '2018-07-09 13:07:53', 'LIFT27', 18, 33, '5.00', 8, 0, 4, 7),
(29, '', 'Prachi Bundela', '', '', '9926874679', '123456', 'I4UHZA', 'LIFT02', 0, '', 0, '1234', 1, '', '', 'android', '1234567890bbbbbbb', 'f6sf46s5f465s4f6s4f6sd4f6ds5f4s6f4sd6f4sd6f4sd65fsd4f65sd4f645sdf645as', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-06-30 12:35:12', '2018-07-04 13:15:11', 'LIFT29', 0, 0, '0.00', 0, 0, 0, 0),
(32, '', 'Jaipal', '', '', '9617822421', 'abc123', 'GUPPXD', 'LIFT02', 1, '', 1, '', 1, '', 'ZY223HXKTT', 'android', '4200aac3dab8745f', '', '10', '', '', '', '', '', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', '2018-06-30 09:07:19', '2018-07-08 11:15:11', 'LIFT32', 5, 4, '0.00', 16, 0, 0, 5),
(33, '228969681247888', 'Kamlesh Shrivastava', '', '', '9229900141', '9229900141', '', '', 2, 'https://graph.facebook.com/228969681247888/picture?type=large', 1, '', 1, 'b011ff4db1c7abe1d1bc7d4fc8c8d174c1eb061e1530429894273', 'FAAZCY130520', 'android', 'FAAZCY130520', 'd6a1MCfwlVI:APA91bEDxXNcV07OEEn7qXICxRXSha7o8vmoLS0lhaUgOwzlncC5O1fhYTTJFmivqR0fgH6L8rOxZP1dK6Tf3dDC7DOKPgZuYCPsFDTXC3sevY75D1OY9oV10IAog8pJh2B44RLOdQCmjGoUU4lnDMhSDqrSzVpoYg', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-07-01 12:53:24', '2018-07-04 13:15:11', 'LIFT33', 0, 1, '0.00', 0, 0, 0, 0),
(34, '', 'Anil Gupta', '', '', '9893261990', '123456', 'DHQICC', '', 1, '', 1, '', 1, 'e5d685bf125565b9faf3affde731474693fa38101530443352491', '6c19d557d440', 'android', '6c19d557d440', 'fhege-CwJMI:APA91bE0P-CEZDcmuwvjSP6j03EDdx-JAiN4Ov1hVVeBSaAr6cKSKhNs0j_jWxgbBkzxvFXlhVy4T_FJsHMQBBmxb_Kgv54BLvmZGicPgjB6V6_DMFw16PdcQrdhdY0tZXl319qz_MTA4lYPPIOU24BDfJPSPkpfow', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-07-01 04:38:45', '2018-07-04 13:15:11', 'LIFT34', 0, 0, '0.00', 0, 0, 0, 0),
(35, '', 'Yatindra', '', '', '9754743271', '123456', '6GQVYZ', 'LIFT02', 2, '', 1, '', 1, '353ee51caf0f7469cc2235ec0484d0ab9963be971531073009774', 'HKE80HWF', 'android', 'HKE80HWF', 'cVto0qRjOic:APA91bFqkG8_kxdjHZmaNxHSFym_tBWlG56CAxTfsJjH7x4FDvzReV3t1mMg5aWwsvj2eeUlNL5wIhcXsJaWVjjVaAqUL4wv2GEGnSA4x-9VFIZFfCou1om1aIP2tpt2rOcE55xKSri2tlRD9qJKqTCRqBoC5A3Dqw', '10', '', '', '', '', '', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', '2018-07-08 11:33:22', '2018-07-09 12:41:43', 'LIFT35', 3, 1, '0.00', 3, 0, 3, 7),
(36, '861192600746174', '', '', 'monikaagupta17@gmail.com', '', '', '', '', 0, '', 0, '', 1, '', '', 'android', 'W49TFY5TWOUWCQQ8', '', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-07-09 01:42:14', '2018-07-09 08:18:08', 'LIFT36', 0, 0, '0.00', 0, 0, 0, 0),
(37, '', 'Monika', '', '', '8120348433', '123456', '2UAAVM', '', 2, '', 1, '', 1, '670a18d9f3056e63171d1e2e7f1f2b46c69271b11531138109598', 'W49TFY5TWOUWCQQ8', 'android', '54358ec6', 'dVzspHEipAA:APA91bEMTcSXtsn0RE7ysKHSvB0gZUd_mAYpjP0sCLtSug5FruU6I7-JYDHQaYHczOJSpzC--aXdjavAKtGJNRV-2XLmHoDw0iWizH7-qF51NM90fT9TLIcTCJ-U80wdT1ra8XNpqHDRQTqhZQS_GaKvCznylq_z3g', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-07-09 01:48:02', '2018-07-09 13:07:53', 'LIFT37', 1, 10, '0.00', 0, 16, 4, 7),
(38, '', 'Harshita', '', '', '8878725953', '123456789', 'NXTP8V', '', 1, '', 1, '', 1, '39735bf7b26e23a248bbca0ce2b066917b6d02551531129884670', '2654d4e57d34', 'android', '2654d4e57d34', 'cXrv77XrVUM:APA91bHd2KIxZd6RysF045G_zZBZzsHq-80Cy3oY58KLh5NZpmF-TGIIOzqKCQlXk3mFTik3PFoZXQQZ__2o5twaZihm1zxgMAidWzPkWJDgAscHIf7jKRi9c8D16uJ-_IVI6RmUbTN79uLvWGM7BSbw0bUXTkC4dw', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-07-09 03:20:57', '2018-07-09 11:02:21', 'LIFT38', 0, 14, '0.00', 0, 0, 7, 0),
(39, '', 'Harshi', '', '', '8770810110', '123456', 'RUDL2Z', '', 1, '', 1, '', 1, '8f0e7ef1c14ba522bbcfe321a82b98fec0c1f0bc1531134594387', 'PL2GAR4813014799', 'android', 'PL2GAR4813014799', 'd54Ooq_ew4w:APA91bFDe0GHLQUzNOZcZyTwIx4jeMz3qfeE3SdpGvslIu6ag7m-IWmlZOAVp53BWp7aPjn2AnCbg4uy_Y5pTIJMJSvxTflaC-nlSHI6Ozn3SacergmN8q-nK0cyY5vEx97211ohUDVLDQCu2qILAMKh3ecpbcG-qA', '10', '', '', '', '', '', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', '2018-07-09 04:39:50', '2018-07-09 13:06:42', 'LIFT39', 3, 7, '0.00', 7, 0, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `vehicle_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `vehicle_model` varchar(30) NOT NULL,
  `vehicle_registration_num` varchar(30) NOT NULL,
  `vehicle_image` varchar(200) NOT NULL,
  `vehicle_registration_image` varchar(200) NOT NULL,
  `vehicle_type` int(1) NOT NULL COMMENT '1=bike, 2=car',
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`vehicle_id`, `user_id`, `vehicle_model`, `vehicle_registration_num`, `vehicle_image`, `vehicle_registration_image`, `vehicle_type`, `create_date`, `update_date`) VALUES
(2, 1, 'Activa', 'MP09MM1000', '', '', 1, '2018-03-28 02:29:47', '2018-07-03 09:39:15'),
(4, 1, 'Activa 3G', 'MP09AB1990', 'vimg0_15222439441657.jpeg', 'vrcimg0_15222439441657.jpeg', 1, '2018-03-28 02:32:24', '2018-03-29 05:53:35'),
(23, 7, 'Dummy', 'MP 09 BC 2917', 'vimg_15229142805037.jpeg', 'vrcimg_15229142805037.jpeg', 1, '2018-04-05 08:44:40', '2018-04-05 07:44:40'),
(24, 7, 'Model', 'MP 09 AA 9999', 'vimg_15229151961427.jpeg', 'vrcimg_15229151961427.jpeg', 1, '2018-04-05 08:59:56', '2018-04-05 07:59:56'),
(25, 11, 'Hyundai Eon', 'MP 19 CB 2612', 'vimg_152896408045411.jpeg', 'vrcimg_152896406204611.jpeg', 2, '2018-06-13 03:53:01', '2018-06-14 08:14:40'),
(27, 11, 'Honda Activa', 'MP 04 CB 6671', '', '', 1, '2018-06-14 09:13:06', '2018-06-14 08:13:06'),
(28, 24, 'Hsjbbje', 'MP 04 MS 3456', 'vimg_152897485583224.jpeg', 'vrcimg_152897485583224.jpeg', 1, '2018-06-14 12:14:15', '2018-06-14 11:14:15'),
(29, 24, 'Jdhdjka', 'MP 03 KJ 2347', 'vimg_152897494190024.jpeg', 'vrcimg_152897494190024.jpeg', 2, '2018-06-14 12:15:41', '2018-06-14 11:15:41'),
(30, 24, 'Hmahdj', 'MP 34 IT 6662', 'vimg_152897502049924.jpeg', 'vrcimg_152897502049924.jpeg', 1, '2018-06-14 12:17:00', '2018-06-14 11:17:00'),
(37, 2, 'Activa', 'MP09AA1234', '', '', 1, '2018-06-28 12:17:15', '2018-06-28 06:47:15'),
(38, 2, 'Suzuki', 'MP09VC1234', '', '', 2, '2018-06-28 12:18:36', '2018-06-28 06:48:36'),
(39, 27, 'Activa', 'MP09AA1235', '', '', 1, '2018-06-28 03:19:26', '2018-07-03 09:39:04'),
(40, 27, 'Neno', 'MP09AB1236', '', '', 2, '2018-06-28 03:19:46', '2018-06-28 09:49:46'),
(41, 14, 'Activa', 'MP04SH5068', '', '', 1, '2018-06-30 02:56:21', '2018-06-29 21:26:21'),
(42, 32, 'Activa', 'MP09AB1234', '', '', 1, '2018-06-30 09:12:43', '2018-06-30 15:42:43'),
(43, 35, 'Hayate', 'MP099999', '', '', 1, '2018-07-09 03:16:46', '2018-07-09 09:46:46'),
(44, 22, 'Yyt444556', 'MP09FG4456', 'vimg_153113343595922.jpeg', '', 1, '2018-07-09 04:20:35', '2018-07-09 10:50:35'),
(45, 39, '6 RT 78g', 'MO09MO3344', '', '', 1, '2018-07-09 06:36:38', '2018-07-09 13:06:38');

-- --------------------------------------------------------

--
-- Table structure for table `WalletHistory`
--

CREATE TABLE `WalletHistory` (
  `HistoryId` int(10) NOT NULL,
  `UserId` int(10) NOT NULL,
  `OtherUserId` int(10) NOT NULL,
  `Actions` varchar(8) NOT NULL,
  `Msg` varchar(200) NOT NULL,
  `Amount` decimal(10,2) NOT NULL,
  `CreateOn` datetime NOT NULL,
  `UpdateOn` datetime NOT NULL,
  `TransactionDetail` varchar(200) NOT NULL,
  `type` int(1) NOT NULL COMMENT '1=reward'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `WalletHistory`
--

INSERT INTO `WalletHistory` (`HistoryId`, `UserId`, `OtherUserId`, `Actions`, `Msg`, `Amount`, `CreateOn`, `UpdateOn`, `TransactionDetail`, `type`) VALUES
(1, 2, 27, 'Credit', 'You have received 10 rs because your referral code used by', '10.00', '2018-06-13 13:20:11', '2018-06-13 13:20:11', 'ORDER1528892384847', 1),
(2, 2, 27, 'Credit', 'Amount credit in wallet', '1000.00', '2018-06-13 13:20:38', '2018-06-13 13:20:38', 'ORDER1528892416621', 0),
(3, 2, 27, 'Credit', 'Amount credit in wallet', '500.00', '2018-06-13 13:23:50', '2018-06-13 13:23:50', 'ORDER1528892593057', 0),
(4, 11, 0, 'Credit', 'Amount credit in wallet', '50.00', '2018-06-14 10:37:34', '2018-06-14 10:37:34', 'ORDER1528968937595', 0),
(5, 24, 0, 'Credit', 'Amount credit in wallet', '500.00', '2018-06-14 12:37:11', '2018-06-14 12:37:11', 'ORDER1528976112161', 0),
(6, 24, 0, 'Credit', 'Amount credit in wallet', '500.00', '2018-06-14 12:38:45', '2018-06-14 12:38:45', 'ORDER1528976254238', 0),
(7, 2, 0, 'Credit', 'Amount credit in wallet', '100.00', '2018-07-04 11:08:52', '2018-07-04 11:08:52', 'ORDER1530682702474', 0),
(8, 2, 0, 'Credit', 'Amount credit in wallet', '999.00', '2018-07-04 11:14:16', '2018-07-04 11:14:16', 'ORDER1530682863717', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `contact_list`
--
ALTER TABLE `contact_list`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `email_template`
--
ALTER TABLE `email_template`
  ADD PRIMARY KEY (`template_id`);

--
-- Indexes for table `emergency_contact`
--
ALTER TABLE `emergency_contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `end_ride_finder_detail`
--
ALTER TABLE `end_ride_finder_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facebook_friend_list`
--
ALTER TABLE `facebook_friend_list`
  ADD PRIMARY KEY (`friend_id`);

--
-- Indexes for table `find_ride`
--
ALTER TABLE `find_ride`
  ADD PRIMARY KEY (`find_id`),
  ADD KEY `source_lat` (`source_lat`),
  ADD KEY `source_lng` (`source_lng`),
  ADD KEY `destination_lat` (`destination_lat`),
  ADD KEY `destination_lng` (`destination_lng`),
  ADD KEY `end_time` (`end_time`),
  ADD KEY `vehicle_type` (`vehicle_type`);

--
-- Indexes for table `friend_list`
--
ALTER TABLE `friend_list`
  ADD PRIMARY KEY (`friend_id`);

--
-- Indexes for table `offer_ride`
--
ALTER TABLE `offer_ride`
  ADD PRIMARY KEY (`offer_id`),
  ADD KEY `source_lat` (`source_lat`),
  ADD KEY `source_lng` (`source_lng`),
  ADD KEY `destination_lat` (`destination_lat`),
  ADD KEY `destination_lng` (`destination_lng`),
  ADD KEY `end_time` (`end_time`),
  ADD KEY `vehicle_type` (`vehicle_type`),
  ADD KEY `no_of_seats` (`no_of_seats`);

--
-- Indexes for table `rating_review`
--
ALTER TABLE `rating_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ride_detail`
--
ALTER TABLE `ride_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ride_matches`
--
ALTER TABLE `ride_matches`
  ADD PRIMARY KEY (`match_id`),
  ADD KEY `offerer_id` (`offerer_id`),
  ADD KEY `finder_id` (`finder_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support`
--
ALTER TABLE `support`
  ADD PRIMARY KEY (`support_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`vehicle_id`);

--
-- Indexes for table `WalletHistory`
--
ALTER TABLE `WalletHistory`
  ADD PRIMARY KEY (`HistoryId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contact_list`
--
ALTER TABLE `contact_list`
  MODIFY `contact_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;
--
-- AUTO_INCREMENT for table `email_template`
--
ALTER TABLE `email_template`
  MODIFY `template_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `emergency_contact`
--
ALTER TABLE `emergency_contact`
  MODIFY `contact_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `end_ride_finder_detail`
--
ALTER TABLE `end_ride_finder_detail`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `facebook_friend_list`
--
ALTER TABLE `facebook_friend_list`
  MODIFY `friend_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `find_ride`
--
ALTER TABLE `find_ride`
  MODIFY `find_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `friend_list`
--
ALTER TABLE `friend_list`
  MODIFY `friend_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `offer_ride`
--
ALTER TABLE `offer_ride`
  MODIFY `offer_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `rating_review`
--
ALTER TABLE `rating_review`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ride_detail`
--
ALTER TABLE `ride_detail`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ride_matches`
--
ALTER TABLE `ride_matches`
  MODIFY `match_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `support`
--
ALTER TABLE `support`
  MODIFY `support_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `vehicle_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `WalletHistory`
--
ALTER TABLE `WalletHistory`
  MODIFY `HistoryId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
