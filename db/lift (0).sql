-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 22, 2018 at 04:02 PM
-- Server version: 5.7.22
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lift`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(50) NOT NULL,
  `code` varchar(7) NOT NULL,
  `role` int(1) NOT NULL COMMENT 'superadmin=1, admin_user=2',
  `image` varchar(100) NOT NULL,
  `status` int(1) NOT NULL COMMENT '0=active,1=inactive',
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `name`, `email`, `password`, `code`, `role`, `image`, `status`, `create_date`, `update_date`) VALUES
(1, 'Lift', 'lift@gmail.com', 'fcea920f7412b5da7be0cf42b8c93759', '', 1, '', 0, '2017-09-29 00:00:00', '2017-10-17 09:41:41');

-- --------------------------------------------------------

--
-- Table structure for table `contact_list`
--

CREATE TABLE `contact_list` (
  `contact_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `contact_user_id` int(10) NOT NULL,
  `contact_num` varchar(20) NOT NULL,
  `contact_name` varchar(50) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_list`
--

INSERT INTO `contact_list` (`contact_id`, `user_id`, `contact_user_id`, `contact_num`, `contact_name`, `create_date`, `update_date`) VALUES
(98, 14, 11, '9039003919', 'Anant', '2018-06-14 09:15:21', '2018-06-14 09:15:21'),
(99, 14, 16, '9845557199', 'Sumit Sahu', '2018-06-14 09:15:21', '2018-06-14 09:15:21'),
(100, 11, 22, '9713031393', 'Ebabu Uday', '2018-06-14 09:26:09', '2018-06-14 09:26:09'),
(101, 11, 14, '8966842340', 'Monika', '2018-06-14 09:26:09', '2018-06-14 09:26:09'),
(102, 11, 16, '9845557199', 'Sumit Mumbai', '2018-06-14 09:26:09', '2018-06-14 09:26:09'),
(117, 24, 14, '8966842340', 'Monika', '2018-06-15 08:53:47', '2018-06-15 08:53:47'),
(118, 24, 11, '9039003919', 'Anant 2', '2018-06-15 08:53:47', '2018-06-15 08:53:47'),
(119, 24, 16, '9845557199', 'Sumit Bangalore', '2018-06-15 08:53:47', '2018-06-15 08:53:47'),
(120, 2, 10, '7771016011', 'Monti EB', '2018-06-21 07:08:11', '2018-06-21 07:08:11'),
(121, 2, 22, '9713031393', 'Uday Ebabu', '2018-06-21 07:08:11', '2018-06-21 07:08:11'),
(122, 2, 7, '8839649465', 'My Jio', '2018-06-21 07:08:11', '2018-06-21 07:08:11'),
(123, 2, 1, '9754743271', 'Yatindra EB', '2018-06-21 07:08:11', '2018-06-21 07:08:11');

-- --------------------------------------------------------

--
-- Table structure for table `email_template`
--

CREATE TABLE `email_template` (
  `template_id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `page_detail` text NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_template`
--

INSERT INTO `email_template` (`template_id`, `title`, `page_detail`, `create_date`) VALUES
(1, 'About us', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p>\\<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;</p>\n', '2018-03-29 00:00:00'),
(2, 'terms and condition', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p>\\<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;</p>\n', '2018-03-31 00:00:00'),
(3, 'Privacy Policy', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p>\\<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;</p>\n', '2018-03-31 00:00:00'),
(4, 'Return policy', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p>\\<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;</p>\n', '2018-03-31 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `emergency_contact`
--

CREATE TABLE `emergency_contact` (
  `contact_id` int(10) NOT NULL,
  `contact_name` varchar(100) NOT NULL,
  `contact_image` varchar(150) NOT NULL,
  `contact_num` varchar(20) NOT NULL,
  `user_id` int(10) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emergency_contact`
--

INSERT INTO `emergency_contact` (`contact_id`, `contact_name`, `contact_image`, `contact_num`, `user_id`, `create_date`, `update_date`) VALUES
(1, 'Sumit Yadav', '', '812059062', 1, '0000-00-00 00:00:00', '2018-03-28 11:42:54'),
(2, 'jaipal', '', '1111111122', 1, '2018-03-28 12:43:22', '2018-03-28 12:19:52'),
(17, 'Aakash kharcha', 'contact_image_7.jpeg', '8305620438', 7, '2018-03-31 12:07:50', '2018-03-31 11:08:29'),
(20, 'Mummy', '', '9229900141', 11, '2018-04-02 05:14:17', '2018-04-02 04:14:17'),
(21, 'Anant', '', '90390039', 14, '2018-06-14 10:18:55', '2018-06-14 09:18:55'),
(22, 'Sumit Mumbai', '', '9845557199', 11, '2018-06-14 10:46:27', '2018-06-14 09:46:27'),
(23, 'Monika', '', '8966842340', 11, '2018-06-14 10:58:51', '2018-06-14 09:58:51'),
(24, 'Anant 2', 'contact_image_1528971963411_24.jpg', '09039003919', 24, '2018-06-14 11:26:03', '2018-06-14 10:26:03'),
(25, 'Anshul Jio', '', '7987363445', 24, '2018-06-14 11:26:19', '2018-06-14 10:26:19'),
(26, 'Monika Jio', '', '87707879', 24, '2018-06-14 11:26:26', '2018-06-14 10:26:26'),
(27, 'MNJ1T', 'contact_image_1529590066176_2.jpg', '8109525129', 2, '2018-06-21 19:37:46', '2018-06-21 14:07:46');

-- --------------------------------------------------------

--
-- Table structure for table `facebook_friend_list`
--

CREATE TABLE `facebook_friend_list` (
  `friend_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `friend_user_id` int(10) NOT NULL,
  `facebook_id` varchar(50) NOT NULL,
  `facebook_frnd_name` varchar(100) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facebook_friend_list`
--

INSERT INTO `facebook_friend_list` (`friend_id`, `user_id`, `friend_user_id`, `facebook_id`, `facebook_frnd_name`, `create_date`, `update_date`) VALUES
(2, 1, 23, '1111111111111', 'Yash', '2018-04-30 07:25:01', '2018-04-30 07:25:01'),
(3, 1, 7, '1899423096748599', 'sumit yadav', '2018-04-30 07:25:01', '2018-04-30 07:25:01'),
(80, 11, 2, '1899423096748599', 'Sumit Yadav', '2018-06-14 09:25:22', '2018-06-14 09:25:22'),
(84, 2, 11, '10209046604402247', 'Anant Shrivastava', '2018-06-22 12:29:15', '2018-06-22 12:29:15');

-- --------------------------------------------------------

--
-- Table structure for table `find_ride`
--

CREATE TABLE `find_ride` (
  `find_id` int(10) NOT NULL,
  `finder_id` int(10) NOT NULL,
  `source_location` text NOT NULL,
  `destination_location` text NOT NULL,
  `source_lat` varchar(20) NOT NULL,
  `source_lng` varchar(20) NOT NULL,
  `destination_lat` varchar(20) NOT NULL,
  `destination_lng` varchar(20) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `vehicle_type` int(1) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `find_ride`
--

INSERT INTO `find_ride` (`find_id`, `finder_id`, `source_location`, `destination_location`, `source_lat`, `source_lng`, `destination_lat`, `destination_lng`, `start_time`, `end_time`, `vehicle_type`, `create_date`, `update_date`) VALUES
(1, 2, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Chhoti Gwaltoli Main Rd, Siyaganj, Indore, Madhya Pradesh 452007, India', '22.724082100000004', '75.88707099999999', '22.722727', '75.8649566', '2018-06-21 19:17:59', '2018-06-21 19:27:59', 1, '2018-06-19 18:52:59', '2018-06-19 00:00:00'),
(2, 2, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Chhoti Gwaltoli Main Rd, Siyaganj, Indore, Madhya Pradesh 452007, India', '22.724082100000004', '75.88707099999999', '22.722727', '75.8649566', '2018-06-21 19:27:42', '2018-06-21 19:37:42', 1, '2018-06-19 19:02:42', '2018-06-19 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `friend_list`
--

CREATE TABLE `friend_list` (
  `friend_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `user_friend_id` int(10) NOT NULL,
  `request_status` int(1) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `friend_list`
--

INSERT INTO `friend_list` (`friend_id`, `user_id`, `user_friend_id`, `request_status`, `create_date`) VALUES
(1, 3, 2, 1, '2018-06-21 15:26:34'),
(2, 2, 16, 0, '2018-06-22 20:44:25');

-- --------------------------------------------------------

--
-- Table structure for table `offer_ride`
--

CREATE TABLE `offer_ride` (
  `offer_id` int(10) NOT NULL,
  `offerer_id` int(10) NOT NULL,
  `source_location` text NOT NULL,
  `destination_location` text NOT NULL,
  `source_lat` varchar(20) NOT NULL,
  `source_lng` varchar(20) NOT NULL,
  `destination_lat` varchar(20) NOT NULL,
  `destination_lng` varchar(20) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `vehicle_type` int(1) NOT NULL,
  `vehicle_id` int(1) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offer_ride`
--

INSERT INTO `offer_ride` (`offer_id`, `offerer_id`, `source_location`, `destination_location`, `source_lat`, `source_lng`, `destination_lat`, `destination_lng`, `start_time`, `end_time`, `vehicle_type`, `vehicle_id`, `create_date`, `update_date`) VALUES
(2, 3, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Chhoti Gwaltoli Main Rd, Siyaganj, Indore, Madhya Pradesh 452007, India', '22.724082100000004', '75.88707099999999', '22.722727', '75.8649566', '2018-06-19 19:09:30', '2018-06-19 19:19:30', 1, 4, '2018-06-19 18:44:30', '2018-06-19 13:14:30'),
(3, 3, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Chhoti Gwaltoli Main Rd, Siyaganj, Indore, Madhya Pradesh 452007, India', '22.724082100000004', '75.88707099999999', '22.722727', '75.8649566', '2018-06-21 20:26:09', '2018-06-21 20:36:09', 1, 4, '2018-06-21 20:01:09', '2018-06-21 14:31:09'),
(4, 3, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Chhoti Gwaltoli Main Rd, Siyaganj, Indore, Madhya Pradesh 452007, India', '22.724082100000004', '75.88707099999999', '22.722727', '75.8649566', '2018-06-21 20:26:23', '2018-06-21 20:36:23', 1, 4, '2018-06-21 20:01:23', '2018-06-21 14:31:23'),
(5, 3, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Chhoti Gwaltoli Main Rd, Siyaganj, Indore, Madhya Pradesh 452007, India', '22.724082100000004', '75.88707099999999', '22.722727', '75.8649566', '2018-06-21 20:26:44', '2018-06-21 20:36:44', 1, 4, '2018-06-21 20:01:44', '2018-06-21 14:31:44'),
(6, 3, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Chhoti Gwaltoli Main Rd, Siyaganj, Indore, Madhya Pradesh 452007, India', '22.724082100000004', '75.88707099999999', '22.722727', '75.8649566', '2018-06-21 20:28:51', '2018-06-21 20:38:51', 1, 4, '2018-06-21 20:03:51', '2018-06-21 14:33:51'),
(7, 3, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Chhoti Gwaltoli Main Rd, Siyaganj, Indore, Madhya Pradesh 452007, India', '22.724082100000004', '75.88707099999999', '22.722727', '75.8649566', '2018-06-21 20:29:43', '2018-06-21 20:39:43', 1, 4, '2018-06-21 20:04:43', '2018-06-21 14:34:43'),
(8, 3, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Chhoti Gwaltoli Main Rd, Siyaganj, Indore, Madhya Pradesh 452007, India', '22.724082100000004', '75.88707099999999', '22.722727', '75.8649566', '2018-06-21 20:32:12', '2018-06-21 20:42:12', 1, 4, '2018-06-21 20:07:12', '2018-06-21 14:37:12'),
(9, 3, 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', 'Chhoti Gwaltoli Main Rd, Siyaganj, Indore, Madhya Pradesh 452007, India', '22.724082100000004', '75.88707099999999', '22.722727', '75.8649566', '2018-06-21 20:37:14', '2018-06-21 20:47:14', 1, 4, '2018-06-21 20:12:14', '2018-06-21 14:42:14');

-- --------------------------------------------------------

--
-- Table structure for table `rating_review`
--

CREATE TABLE `rating_review` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `booking_id` int(10) NOT NULL,
  `second_user_id` int(10) NOT NULL,
  `rating` int(1) NOT NULL,
  `review` varchar(200) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating_review`
--

INSERT INTO `rating_review` (`id`, `user_id`, `booking_id`, `second_user_id`, `rating`, `review`, `create_date`, `update_date`) VALUES
(1, 1, 1, 0, 3, 'Nice service', '2018-03-28 12:06:38', '2018-03-28 11:08:28');

-- --------------------------------------------------------

--
-- Table structure for table `ride_matches`
--

CREATE TABLE `ride_matches` (
  `match_id` int(10) NOT NULL,
  `offerer_id` int(10) NOT NULL,
  `finder_id` int(10) NOT NULL,
  `ride_type` int(1) NOT NULL COMMENT '1=offer, 2=find',
  `ride_status` int(1) NOT NULL,
  `ride_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `autorejectiontime` varchar(10) NOT NULL,
  `minridetime` varchar(10) NOT NULL,
  `maxridetime` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `autorejectiontime`, `minridetime`, `maxridetime`) VALUES
(1, '', '11', '35'),
(2, '10', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE TABLE `support` (
  `support_id` int(8) NOT NULL,
  `user_id` int(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `support`
--

INSERT INTO `support` (`support_id`, `user_id`, `title`, `description`, `create_date`) VALUES
(1, 1, 'need help', 'How to find driver', '2018-03-29 07:49:05'),
(2, 7, 'Suggestion', 'Testing support message to test support functionality.', '2018-04-04 11:03:09'),
(3, 11, 'Compliment', 'There are many things which are pending..a lot of corrections is to be done', '2018-06-14 11:01:33'),
(4, 24, 'General', 'Ghhejk jehjskk jsjhdlk kshhakkd ijshhakkd kjsghakdb jshh', '2018-06-14 11:21:27'),
(5, 24, 'Suggestion', 'Hhsiid jshhskkf jjsghakkf judggsjks jdjjsbbtk jsjhsj', '2018-06-14 11:22:24'),
(6, 24, 'Compliment', 'Hhsggaj bdghdk jsvgdkd kjsvbsk jsbhdkkdv jshhskks jjdh', '2018-06-14 11:23:18'),
(7, 24, 'Question', 'Hhdjk hshjdk jsghskkf jjsgyskk jshhskks jjdgjskbjsyhsk', '2018-06-14 11:23:43');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(10) NOT NULL,
  `facebook_id` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_facebook_id` varchar(100) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_mobile` varchar(20) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_refer_code` varchar(10) NOT NULL COMMENT 'self',
  `user_referal_code` varchar(10) NOT NULL COMMENT 'other',
  `user_gender` int(1) NOT NULL COMMENT '1=male,2=female',
  `user_image` varchar(200) NOT NULL,
  `user_mobile_status` int(1) NOT NULL COMMENT '1=active',
  `mobile_code` int(4) NOT NULL,
  `admin_status` int(1) NOT NULL COMMENT '1=active',
  `user_token` varchar(100) NOT NULL,
  `registered_device_id` varchar(100) NOT NULL,
  `user_device_type` varchar(10) NOT NULL,
  `user_device_id` varchar(100) NOT NULL,
  `user_device_token` text NOT NULL,
  `user_wallet` varchar(15) NOT NULL,
  `user_lat` varchar(25) NOT NULL,
  `user_lng` varchar(15) NOT NULL,
  `user_home_location` varchar(255) NOT NULL,
  `user_work_location` varchar(255) NOT NULL,
  `user_other_location` varchar(255) NOT NULL,
  `user_home_lat` varchar(25) NOT NULL,
  `user_home_lng` varchar(25) NOT NULL,
  `user_work_lat` varchar(25) NOT NULL,
  `user_work_lng` varchar(25) NOT NULL,
  `user_other_lat` varchar(25) NOT NULL,
  `user_other_lng` varchar(25) NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `corporate_email` varchar(200) NOT NULL,
  `emergancy_contact` varchar(20) NOT NULL,
  `profession` varchar(100) NOT NULL,
  `about` varchar(200) NOT NULL,
  `user_license_num` varchar(30) NOT NULL,
  `license_image` varchar(150) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Lift_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `facebook_id`, `user_name`, `user_facebook_id`, `user_email`, `user_mobile`, `user_password`, `user_refer_code`, `user_referal_code`, `user_gender`, `user_image`, `user_mobile_status`, `mobile_code`, `admin_status`, `user_token`, `registered_device_id`, `user_device_type`, `user_device_id`, `user_device_token`, `user_wallet`, `user_lat`, `user_lng`, `user_home_location`, `user_work_location`, `user_other_location`, `user_home_lat`, `user_home_lng`, `user_work_lat`, `user_work_lng`, `user_other_lat`, `user_other_lng`, `company_name`, `corporate_email`, `emergancy_contact`, `profession`, `about`, `user_license_num`, `license_image`, `create_date`, `update_date`, `Lift_id`) VALUES
(1, '', 'y43', '', '', '9754743271', '123456', 'R26CK5', '', 1, '', 1, 0, 1, '', '12345678901111111', 'android', '54358ec6', '', '20', '', '', 'marimata square', '616, shekhar central', 'banganga indore', '22.123456', '75.123456', '22.245648', '75.245648', '25.864864', '75.864864', 'Engineer babu', 'yatindra.mohite@ebabu.co', '', 'Web Developer', 'I am a multi tasking banda', 'Lic.123.43', '', '2018-03-19 08:15:45', '2018-06-21 12:09:47', 'LIFT01'),
(2, '1899423096748599', 'Sumit Yadav', '', '', '8109059062', '123456', '8NEPJC', 'R26CK5', 0, '', 1, 1234, 1, '4436a577815b6f93cdccc93c892692ff2cfd079c1529587351230', '1234567890aaaaaaaa', 'android', 'ZY223HXKTT', 'eQxd1qVqHGo:APA91bHZDcJTJ4fhoerKDJO0wnl5_K2nIK2J8T_b91Qljp8HXty0cjQrp82QWTUdinbBhdUaALVqHYzQk_oD33iubc7PUY2LCYFnnpPQVyzcHpy9D4RCIG43KsuSxbZnp6RJ5hj5NP3E', '2010', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-19 08:30:27', '2018-06-21 13:22:31', 'LIFT02'),
(3, '1111111111111', '', '', 'yatindra.mohite@ebabu.co', '', '', '', '', 0, '', 1, 0, 1, '0b1d1ecf9673441f75bf1b9bd038b6c7637e9754152', '1234567890aaaaaa11', 'android', '1234567890aaaaaa11', 'f4s6s66f4f64sf6s46s4f654fs6f4s6fs6f46fs6f4s6s66f', '20', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2018-06-22 13:18:47', 'LIFT03'),
(4, '', 'Sumit', '', '', '8109059063', 'abc123', 'SLEQRS', '', 0, '', 1, 1234, 1, '', '', 'android', 'ZY223HXKTT', '', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-23 01:55:31', '2018-03-27 10:25:22', ''),
(5, '', 'Abc', '', '', '9123456789', 'abc123', 'LQOFOW', '', 0, '', 0, 1234, 1, '', '', 'android', 'ZY223HXKTT', '', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-23 02:21:08', '2018-03-26 05:52:08', ''),
(7, '', 'Sumit', '', 'sumity90@gmail.com', '8839649465', 'abc123', 'SUMIT1', '', 1, 'user_image_1522757267390_7.jpeg', 1, 0, 1, '', 'ZY223HXKTT', 'android', 'ZY223HXKTT', '', '', '', '', 'Chhoti Gwaltoli Main Rd, Siyaganj, Indore, Madhya Pradesh 452007, India', 'Shekhar central, Agra Bombay Rd, Manorama Ganj, Indore, Madhya Pradesh 452018, India', '56 Dukan St, Chappan Dukan, New Palasia, Indore, Madhya Pradesh 452001, India', '22.715963799999994', '75.8649566', '22.722727', '75.88707099999999', '22.724082100000004', '75.8846182', 'Engineerbabu', 'sumit.yadav@ebabu.co', '', 'Engineer', 'Android developer at engineerbabu', 'Mp12345678', 'user_lic_image_1522915325723_7.jpeg', '2018-03-27 08:42:40', '2018-06-01 11:46:40', ''),
(8, '102751848487306351855', '', '', 'sumity90@gmail.com', '', '', '', '', 0, '', 0, 0, 1, '', 'ZY223HXKTT', 'android', 'ZY223HXKTT', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-27 11:49:55', '2018-03-27 10:59:21', ''),
(9, '102783611205631119366', '', '', 'montiparauha812@gmail.com', '', '', 'H7ZZLN', '', 0, '', 0, 0, 1, '', '', 'android', '268cdc1c', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-31 03:09:43', '2018-03-31 14:10:34', ''),
(10, '', 'Monti', '', '', '7771016011', '123456', 'J8DOOB', '', 1, '', 1, 0, 1, 'cb2ef1a26cdec7532b5810e1af68e0c70c3e89061522931519986', '268cdc1c', 'android', '268cdc1c', 'cQPiD_OvTVs:APA91bH3b_HpGKV0HoaT1_cbJ-nJ55WZWrQZxBW090R2ct0EDrH1_fpETP0tQGsavAqlTsx0i7tbEZvInXCMwx2wCJuzUIddX9FPv9jIojG_MVkbDyrx3IDuwuCNoeYNEaQEzOt9Oo43', '20', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-31 03:10:31', '2018-06-21 13:58:14', ''),
(11, '10209046604402247', 'Anant Shrivastava', '', 'anant_abstruse@yahoo.co.in', '9039003919', 'liftowner2', 'RBQLOF', '', 1, 'https://graph.facebook.com/10209046604402247/picture?type=large', 1, 1234, 1, '3d550acb63936cf00f26385d7d84045342271b131528962049392', 'GBAXGY11A389ASN', 'android', 'GBAXGY11A389ASN', 'c9LC5nXCqZs:APA91bEvHdfjX5XjKfl019JkZeKMuCEmMQrldGFaJGLax2Jbktrq_fHpFacg6EXatYzSwLVpIHrqQebnDXy9hfiTqLLWIhJCpblal5wCZ4dmfaExKberBWM_o8MkmPqf7ff4zZz8xJoi', '60', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-31 03:23:15', '2018-06-14 09:44:31', ''),
(12, '', 'Sumit', '', '', '8109059067', 'abc123', 'VZYUDF', '', 1, '', 1, 0, 1, '', 'ZY223HXKTT', 'android', 'ZY223HXKTT', '', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-31 04:18:00', '2018-04-02 05:26:16', ''),
(13, '', 'Bilal', '', '', '9606887838', '12345678', 'QXTDNN', '', 1, '', 1, 0, 1, '125cb0194fc782d7448459d13ad452294b4e94b71522517093894', 'ZY222WT9GF', 'android', 'ZY222WT9GF', 'd9dPD_LqsR0:APA91bEvaPe0hYEXm9rJo8SUsM51I-xPlR-ZeI-sC-oSJ6EVyX89TfDylhH4AYgsL3N1oOB8nrG2W9AUJRdLbtvJRT1bkKKTTwcuWVXSaDPYie-I9BMXebUIew38nSU1h1-Az7yB60ZQ', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-31 06:24:50', '2018-03-31 17:24:53', ''),
(14, '', 'Monika Hassija', '', '', '8966842340', 'moni1990has', 'WYWWJD', '', 2, '', 1, 0, 1, 'abd5eb8602c4096b320206ee9583138301b699a01528963527098', '493b2663', 'android', '493b2663', 'ePSXSHdk0JU:APA91bGQ9NmkoATDM4PRNdRc2h8jjT9jXrPL5gG3_EqVHJqkytODEVz2bgQEfgDPXClJ1SAP0A5siD1bm2AwbTuqmt1XMpPi0wW1Qfyz7h30FRcAssu-Xq_oTsz0DfyOIbpWUBYLFemh', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-04-02 05:41:39', '2018-06-14 08:05:27', ''),
(15, '113552304616879986908', '', '', 'sumit.yadav@ebabu.co', '', '', 'U8VWTL', '', 0, '', 0, 0, 1, '', '', 'android', 'ZY223HXKTT', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-04-10 10:40:40', '2018-05-11 07:35:37', ''),
(16, '110409204563740063768', 'Sumit', '', 'sumitsahu123@gmail.com', '9845557199', 'guddodi15', '61IAHU', '', 1, '', 1, 0, 1, 'e7540f8667010df9f5b44b98f45b470c12e714201527435317749', '05fbdb400ae56c29', 'android', '05fbdb400ae56c29', 'dhK0PivY3iY:APA91bGwYKGr97PGxdmHsAF_WC9dpCHzuO6dk9Qo2GdvgbzMA-zET53XGKPHX17BmI-GUChGPq9G6TJ_BJ4KcZoyg51MPd_FJw2C_KFHeCcwFNiKP_b-83fqqinrZPqb33S6oFbKaEgk', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-04-10 06:35:05', '2018-06-22 14:35:07', 'LIFT16'),
(17, '137142860462149', '', '', 'remo_egijanr_remo@tfbnw.net', '', '', 'W3J6HA', '', 0, '', 0, 0, 1, '', '', 'android', '54358ec6', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-04-11 11:16:10', '2018-05-18 04:55:18', ''),
(18, '107819113407654', '', '', 'jaipal_rjtconz_solanki@tfbnw.net', '', '', 'GNM5YX', '', 0, '', 0, 0, 1, '', '', 'android', '54358ec6', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-04-11 11:35:01', '2018-05-18 04:55:18', ''),
(19, '110581479797258', '', '', 'monika_dnczhrz_gupta@tfbnw.net', '', '', 'FFH4TN', '', 0, '', 0, 0, 1, '', '', 'android', '54358ec6', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-04-11 11:36:19', '2018-05-18 04:55:18', ''),
(20, '122229388629953', '', '', 'harsh_izwgvas_parashar@tfbnw.net', '', '', 'BCDPA7', '', 0, '', 0, 0, 1, '', '', 'android', '54358ec6', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-04-11 11:37:26', '2018-05-18 04:55:18', ''),
(21, '116497657947397447802', 'Ghf', '', 'paytmrisk.assessment@gmail.com', '8800548368', 'tyagi123', 'UYMWGY', '', 1, '', 0, 1234, 1, '', '', 'android', 'ZW2223XTNG', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-05-25 12:00:21', '2018-05-31 09:58:34', ''),
(22, '', 'Uday', '', '', '9713031393', 'abc123', '7LNEIV', '', 1, '', 1, 0, 1, '1d54b98b8935b173633dd9b8dd738db0e4bc3fc01527604883237', '54358ec6', 'android', '42002c66d88d44c5', 'drHaDHFkv70:APA91bEg_EdzzD-3SSzYNVfbMwiBGZFOnYVkAgz9hZbPM13pp_7Qsu5eQa1482mVXUt4_3bMonl79ulrk83vSWFfgDH2hj-T7UOuJLc_nOWnIkW2euwD0_zAF6s4YUnlyYbjDWd5TsTI', '10', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-05-29 11:40:26', '2018-05-29 14:41:23', ''),
(23, '', '', '', 'sumity90@gmail.com', '', '', 'LVSCO6', '', 0, '', 0, 0, 1, '', '', 'android', 'ZY223HXKTT', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-06-01 02:51:27', '2018-06-05 05:24:40', ''),
(24, '', 'Nishant', '', '', '9713690098', '123456', 'MWIVU9', 'RBQLOF', 1, 'user_image_1528972924607_24.jpeg', 1, 0, 1, '369bd1ce2d51a685ed6d18f4b2bb86556632bc441528973744595', 'QS7HQWROAIKVJFEE', 'android', 'QS7HQWROAIKVJFEE', 'eQTvUni4D9Y:APA91bFs8eCSjGMlwp0Z7uL50XTZ1UPn0zyJqDgA7R0RdOkSeBZuWqHOAfA0vuRqatyqAQVP5dKv9FEarkJEbPA_qfMjQzXUrkyoEedUtCLAyqCaUuQHa39aJHrXGsinISr418TWK4QN', '1010', '', '', 'Bhopal Rd, Shala, Madhya Pradesh 466331, India', 'House no 133, 5th Cross, Ranganath colony, Opp BHEL., Mysore Rd, Ranganathan Colony, Deepanjali Nagar, Bengaluru, Karnataka 560026, India', 'Hinjewadi Ph-2 Rd, Phase 2, Hinjewadi Rajiv Gandhi Infotech Park, Hinjawadi, Pimpri-Chinchwad, Maharashtra 411057, India', '22.690516199999998', '77.2947057', '12.9465294', '77.53587089999999', '18.594330400000004', '73.7095995', 'Hdjdjj', 'gehshdb@gshsh.com', '', 'Jdjdjj', 'Hdhdbje hshwh jdjei jwiwo jdjd isuej jdowkkfj', 'Dghd52618gsh6262727', 'user_lic_image_1528972924607_24.jpeg', '2018-06-14 10:44:31', '2018-06-14 11:38:45', ''),
(25, '11111111111122', '', '', 'yatindra.mohite43@ebabu.co', '', '', 'CY51IX', '', 0, '', 0, 0, 1, '', '', 'android', '1234567890aaaaaa1122', 'f4s6s66f4f64sf6s46s4f654fs6f4s6fs6f46fs6f4s6s66f', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-06-21 07:23:02', '2018-06-21 13:53:02', 'LIFT25');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `vehicle_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `vehicle_model` varchar(30) NOT NULL,
  `vehicle_registration_num` varchar(30) NOT NULL,
  `vehicle_image` varchar(200) NOT NULL,
  `vehicle_registration_image` varchar(200) NOT NULL,
  `vehicle_type` int(1) NOT NULL COMMENT '1=bike, 2=car',
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`vehicle_id`, `user_id`, `vehicle_model`, `vehicle_registration_num`, `vehicle_image`, `vehicle_registration_image`, `vehicle_type`, `create_date`, `update_date`) VALUES
(2, 1, 'Activa', 'MP09MM1000', '', '', 1, '2018-03-28 02:29:47', '2018-03-29 05:53:28'),
(4, 1, 'Activa 3G', 'MP09AB1990', 'vimg0_15222439441657.jpeg', 'vrcimg0_15222439441657.jpeg', 1, '2018-03-28 02:32:24', '2018-03-29 05:53:35'),
(22, 2, 'Activa 3G', 'MP 09 BB 1234', 'vimg_15226470317882.jpeg', 'vrcimg_15226470317882.jpeg', 1, '2018-04-02 06:30:31', '2018-04-02 05:30:31'),
(23, 7, 'Dummy', 'MP 09 BC 2917', 'vimg_15229142805037.jpeg', 'vrcimg_15229142805037.jpeg', 1, '2018-04-05 08:44:40', '2018-04-05 07:44:40'),
(24, 7, 'Model', 'MP 09 AA 9999', 'vimg_15229151961427.jpeg', 'vrcimg_15229151961427.jpeg', 1, '2018-04-05 08:59:56', '2018-04-05 07:59:56'),
(25, 11, 'Hyundai Eon', 'MP 19 CB 2612', 'vimg_152896408045411.jpeg', 'vrcimg_152896406204611.jpeg', 2, '2018-06-13 03:53:01', '2018-06-14 08:14:40'),
(27, 11, 'Honda Activa', 'MP 04 CB 6671', '', '', 1, '2018-06-14 09:13:06', '2018-06-14 08:13:06'),
(28, 24, 'Hsjbbje', 'MP 04 MS 3456', 'vimg_152897485583224.jpeg', 'vrcimg_152897485583224.jpeg', 1, '2018-06-14 12:14:15', '2018-06-14 11:14:15'),
(29, 24, 'Jdhdjka', 'MP 03 KJ 2347', 'vimg_152897494190024.jpeg', 'vrcimg_152897494190024.jpeg', 2, '2018-06-14 12:15:41', '2018-06-14 11:15:41'),
(30, 24, 'Hmahdj', 'MP 34 IT 6662', 'vimg_152897502049924.jpeg', 'vrcimg_152897502049924.jpeg', 1, '2018-06-14 12:17:00', '2018-06-14 11:17:00'),
(31, 2, 'Maruti', 'MP 09 AB 1234', 'vimg_15293095246652.jpeg', 'vrcimg_15293095246652.jpeg', 2, '2018-06-18 09:12:04', '2018-06-18 08:12:04');

-- --------------------------------------------------------

--
-- Table structure for table `WalletHistory`
--

CREATE TABLE `WalletHistory` (
  `HistoryId` int(10) NOT NULL,
  `UserId` int(10) NOT NULL,
  `Actions` varchar(8) NOT NULL,
  `Msg` varchar(200) NOT NULL,
  `Amount` decimal(10,2) NOT NULL,
  `CreateOn` datetime NOT NULL,
  `UpdateOn` datetime NOT NULL,
  `TransactionDetail` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `WalletHistory`
--

INSERT INTO `WalletHistory` (`HistoryId`, `UserId`, `Actions`, `Msg`, `Amount`, `CreateOn`, `UpdateOn`, `TransactionDetail`) VALUES
(1, 2, 'Credit', 'Amount credit in wallet', '500.00', '2018-06-13 13:20:11', '2018-06-13 13:20:11', 'ORDER1528892384847'),
(2, 2, 'Credit', 'Amount credit in wallet', '1000.00', '2018-06-13 13:20:38', '2018-06-13 13:20:38', 'ORDER1528892416621'),
(3, 2, 'Credit', 'Amount credit in wallet', '500.00', '2018-06-13 13:23:50', '2018-06-13 13:23:50', 'ORDER1528892593057'),
(4, 11, 'Credit', 'Amount credit in wallet', '50.00', '2018-06-14 10:37:34', '2018-06-14 10:37:34', 'ORDER1528968937595'),
(5, 24, 'Credit', 'Amount credit in wallet', '500.00', '2018-06-14 12:37:11', '2018-06-14 12:37:11', 'ORDER1528976112161'),
(6, 24, 'Credit', 'Amount credit in wallet', '500.00', '2018-06-14 12:38:45', '2018-06-14 12:38:45', 'ORDER1528976254238');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `contact_list`
--
ALTER TABLE `contact_list`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `email_template`
--
ALTER TABLE `email_template`
  ADD PRIMARY KEY (`template_id`);

--
-- Indexes for table `emergency_contact`
--
ALTER TABLE `emergency_contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `facebook_friend_list`
--
ALTER TABLE `facebook_friend_list`
  ADD PRIMARY KEY (`friend_id`);

--
-- Indexes for table `find_ride`
--
ALTER TABLE `find_ride`
  ADD PRIMARY KEY (`find_id`);

--
-- Indexes for table `friend_list`
--
ALTER TABLE `friend_list`
  ADD PRIMARY KEY (`friend_id`);

--
-- Indexes for table `offer_ride`
--
ALTER TABLE `offer_ride`
  ADD PRIMARY KEY (`offer_id`);

--
-- Indexes for table `rating_review`
--
ALTER TABLE `rating_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support`
--
ALTER TABLE `support`
  ADD PRIMARY KEY (`support_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`vehicle_id`);

--
-- Indexes for table `WalletHistory`
--
ALTER TABLE `WalletHistory`
  ADD PRIMARY KEY (`HistoryId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contact_list`
--
ALTER TABLE `contact_list`
  MODIFY `contact_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;
--
-- AUTO_INCREMENT for table `email_template`
--
ALTER TABLE `email_template`
  MODIFY `template_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `emergency_contact`
--
ALTER TABLE `emergency_contact`
  MODIFY `contact_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `facebook_friend_list`
--
ALTER TABLE `facebook_friend_list`
  MODIFY `friend_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `find_ride`
--
ALTER TABLE `find_ride`
  MODIFY `find_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `friend_list`
--
ALTER TABLE `friend_list`
  MODIFY `friend_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `offer_ride`
--
ALTER TABLE `offer_ride`
  MODIFY `offer_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `rating_review`
--
ALTER TABLE `rating_review`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `support`
--
ALTER TABLE `support`
  MODIFY `support_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `vehicle_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `WalletHistory`
--
ALTER TABLE `WalletHistory`
  MODIFY `HistoryId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
