<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

	class Check_token extends CI_Model {
	public function __construct()
    {
        parent::__construct();
	}
	
	function check_authentication()
	{
	    $response = '';
	 	$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$final_output['data'] = $check['data'];
				$final_output['status'] = "true";
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['status'] ="false";
			$final_output['message'] = "Unauthorised access";
		}
	    return $final_output;	
	}
	
	function ChechAuth($token)
	{
		$auth = $this->common_model->getDataField('user_id,admin_status,user_image','barber_user',array('user_token'=>$token));
		if(!empty($auth))
		{
			$abc['status'] = "true";
			$abc['data'] = $auth[0];
			return $abc;
		}else
		{
			$abc['status'] = "false";
			return $abc;
		}
	}

}

?>