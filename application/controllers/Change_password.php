<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Change_password extends MY_Controller {
function __construct() {
		parent::__construct();
		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
			
		/*if($this->check_authentication() != 'success')
        die;*/
	}

	function account($id='')
	{
		if(isset($_POST['submit']))
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[4]|max_length[40]');
			$this->form_validation->set_rules('confrm_password', 'Confirm Password', 'trim|required|matches[new_password]');
			$ids = $this->common_model->encryptor_ym('decrypt',$id);
			$new = $this->input->post('new_password');
			$get = $this->db->select('user_id,pass_code')->get_where('qalame_user',array('user_id'=>$ids))->row();
			if(!empty($get))
			{
				if($get->pass_code != '' && strlen($get->pass_code)==6)
				{
					if($this->form_validation->run() == TRUE)
					{
						$update = $this->common_model->updateData("qalame_user",array('user_password'=>md5($new),'pass_code'=>'','update_date'=>date("Y-m-d H:i:s")),array('user_id'=>$ids));
						$this->session->set_flashdata("success",'Password has been successfully changed.');	
						redirect('change_password/account');
					}
				}else
				{
					$message = $this->load->view('email_template/verification_failed.php','',TRUE);
					print_r($message);exit;
				}
			}else
			{
				$message = $this->load->view('email_template/verification_failed.php','',TRUE);
				print_r($message);exit;
			}
		}

		$this->load->view('change_password');
	}

}