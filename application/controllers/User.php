<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
	}
	
	public function verified()
	{
		$this->db->cache_on();

		$data['user_data'] = $this->common_model->getData('qalame_user',array('user_status'=>'1','admin_status'=>'1'),'user_id','DESC');

		$this->db->cache_off();
		$this->load->view('admin/user/verified_user',$data);
	}

	public function unverified()
	{

		$data['user_data'] = $this->common_model->getData('qalame_user',array('user_status'=>'0','admin_status'=>'1'),'user_id','DESC');
		
		$this->load->view('admin/user/unverified_user',$data);
	}

    //Deactivate action for verified and unverified 
	public function deactivate_user()
	{
		$user_id = $this->input->post('user_id');
        $delete = $this->db->query("UPDATE `qalame_user` SET `admin_status` = 0 WHERE `user_id` IN($user_id)");

        if($delete)
        {
        	echo $user_id;exit;
        }	
	}

    //Listing of Deactive Users
	public function deactivate()
	{
		$data['user_data'] = $this->common_model->getData('qalame_user',array('admin_status'=>'0'),'user_id','DESC');
		$this->load->view('admin/user/deactivate_user',$data);
	}

    //Active action for deactive user
	public function activate_user()
	{
        $user_id = $this->input->post('user_id');
        $delete = $this->db->query("UPDATE `qalame_user` SET `admin_status` = 1 WHERE `user_id` IN($user_id)");

        if($delete)
        {
        	echo $user_id;exit;
        }
	}

	public function delete()
	{
	   $user_id = $this->input->post('user_id');

	   $delete = $this->db->query("DELETE FROM `qalame_user` WHERE `user_id` IN($user_id)");
	   $delete = $this->db->query("DELETE FROM `qalame_follow_user` WHERE `following_id` IN($user_id)");

	   $delete = $this->db->query("DELETE FROM `qalame_follow_user` WHERE `follower_id` IN($user_id)");

	   $delete = $this->db->query("DELETE FROM `qalame_user_post` WHERE `user_id` IN($user_id)");

	   echo $user_id;exit;

	}

	public function getfollowername($user_id = false)
	{  
		$arr = array();

		$followers11 = $this->common_model->getData('qalame_follow_user',array('following_id'=>$user_id,'request_status'=>1),'follow_id','DESC');

		if(!empty($followers11))
		{
			foreach($followers11 as $follower)
			{
                $followerdata = $this->common_model->common_getRow('qalame_user',array('user_id'=>$follower->follower_id));

                $arr[] = array('user_name'=>$followerdata->user_name,
                			  'user_email'=>$followerdata->user_email,	
                			  'user_image'=>$followerdata->user_image,
                			  'user_gender'=>$followerdata->user_gender,
                			  'user_location'=>$followerdata->user_location,
                			  'create_date'=>$follower->create_date
                	        );

			}
		}
		 $data['followers'] = $arr;

		$this->load->view('admin/user/followersview',$data);	
	}
	
}
