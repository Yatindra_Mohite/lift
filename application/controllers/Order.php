<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Order extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		//if(!$userid = $this->session->userdata('admin_id')){
			//redirect(base_url('login'));
		//}

     // $response = $this->common_model->check_auth($this->session->userdata('admin_id'));
     // if($response == 1001)
      //{
       // redirect(base_url().'Logout');
    //  }    
  }

  // public function index()
  // {
  //   $data['products'] = $this->common_model->getData('Product',array(),'id','DESC');

  //   $this->load->view('admin/product/product',$data);
  // }

  public function index()
  { 
          $newarr = array(1=>'IsInBreakFast',2=>'IsInLunch',3=>'IsInLunch');
          $daytimearr = array(1=>'BreakFast',2=>'Lunch',3=>'Dinner');
         // $day_number = date('N', strtotime(date('Y-m-d',strtotime(' +1 day'))));
          $arr = $arr1 = $arr2 = $arr3= $narr =''; 
           $day_number = 5;
            //get data from admin menu (main data)
           	for ($i=1; $i < 4 ; $i++) { 

           		$id = $daytimearr[$i]; $userids = array();
           		$getonstatus = $this->common_model->getDataField("UserId","TiffinStartEndStatus",array($id => 1,'WeekDay'=>$day_number));
           	  
            	if(!empty($getonstatus))
           		{
                  foreach ($getonstatus as $key) {
                    $userids[] = $key->UserId;

                  }
                //   if($i==1){
                //     $arr1= $getonstatus[0]->userid;
                //     $selec_max_cate_b = $this->db->query("SELECT CategoryId,SubCategoryId,SUM(Quantity) as maxcount  FROM OrderItems WHERE DayTime = 1 AND WeekDay = $day_number GROUP BY SubCategoryId limit 1")->row();
                //     if(!empty($selec_max_cate_b))
                //     {
                //       $max_cate_breakfast[] = $selec_max_cate_b;
                //     }                
             			// }elseif($i==2){
             			// 	$arr2= $getonstatus[0]->userid;
                //     $selec_max_cate_l = $this->db->query("SELECT Product.CategoryId,SubCategoryId,SUM(Quantity) as maxcount  FROM OrderItems INNER JOIN Product ON OrderItems.SubCategoryId=Product.Id WHERE DayTime = 2 AND WeekDay = '$day_number' GROUP BY Product.CategoryId")->result();
                //     if(!empty($selec_max_cate_l))
                //     {
                //         $max_cate_lunch = $selec_max_cate_l;
                //     }
                //   }
             			// else{
             			// 	$arr3= $getonstatus[0]->userid;
                //     $selec_max_cate_d = $this->db->query("SELECT Product.CategoryId,SubCategoryId,SUM(Quantity) as maxcount  FROM OrderItems INNER JOIN Product ON OrderItems.SubCategoryId=Product.Id WHERE DayTime = 3 AND WeekDay = '$day_number' GROUP BY Product.CategoryId")->result(); 
                //     if(!empty($selec_max_cate_d))
                //     {
                //         $max_cate_dinner = $selec_max_cate_l;
                //     }
                //   }
           		}
          

            $breakfast = $lunch = $dinner = array();
            $get_admin_menu = $this->db->query("SELECT GROUP_CONCAT(tps.ProductId) as ProductId,ProductCategory.Id as cateid,ProductCategory.CategoryName FROM TiffenProductSchedule as tps INNER JOIN Product ON (tps.ProductId = Product.Id ) INNER JOIN ProductCategory ON Product.CategoryId = ProductCategory.Id WHERE tps.WeekDay = $day_number AND tps.".$newarr[$i]." = 1 AND tps.IsEnabled = 1 GROUP BY ProductCategory.Id ORDER BY tps.Id ASC ")->result(); //get product according admin if user not select_address
            if(!empty($get_admin_menu))
            {
        		  $cate1 = $cate2 = $cate3 = $cate4 = array();      
              foreach ($get_admin_menu as $admin_value) {
                $admin_value->Quantity = "0";
                $cid = $admin_value->cateid;
                $narr['cate_'.$cid] = array();
                  $explode = explode(',', $admin_value->ProductId);
                  foreach ($explode as $value) {
                        $narr['cate_'.$cid][] = array(
                                                      'prod_id'=>$value,
                                                      'count_'.$value =>0
                                                      );
                    }

                 //print_r(array_key_exists("count_99",$narr['cate_'.$cid]));exit;
                if(!empty($userids))
                {
                  $impusers = implode(',',$userids);
                  $get_product_data = $this->db->query("SELECT OrderItems.food_status,OrderItems.Quantity,Product.Name,Product.Amount,Product.Id,Product.CategoryId FROM OrderItems INNER JOIN Product ON (OrderItems.SubCategoryId = Product.Id ) WHERE OrderItems.CategoryId = '".$admin_value->cateid."' AND OrderItems.DayTime = $i AND OrderItems.WeekDay = $day_number AND Product.Status = 1 AND OrderItems.UserId IN (".$impusers.")")->result(); //get product according user requirement and also available in admin
                  if(!empty($get_product_data))
                  {
                      foreach ($get_product_data as $keyvalue) {
                          if($keyvalue->food_status==1)
                          {
                            $narr['cate_'.$cid]['prod_id'] = $narr['cate_'.$cid]['prod_id']+$keyvalue->Id;
                            $narr['cate_'.$cid]['count_'.$keyvalue->Id] = $narr['cate_'.$cid]['count_'.$keyvalue->Id]+$keyvalue->Quantity;
                          }elseif($keyvalue->food_status==2)
                          {
                            $narr['cate_'.$cid][] = array(
                                                      'prod_id'=>$keyvalue->Id,
                                                      'count_'.$keyvalue->Id=>$keyvalue->Quantity
                                                      );
                          }
                          print_r(json_encode($narr));
                      } exit;
                  }
                }
              }  
            } 

            
            //$final_arr[] = array($daytimearr[$i]=>$narr);
          } 
  	
  }


  public function manage_menu()
  { 
    $data['products'] = $this->db->query("SELECT cate.CategoryName,cate.Id,GROUP_CONCAT(Product.Name) as proname,GROUP_CONCAT(Product.Id) as proid FROM `Product` INNER JOIN ProductCategory as cate ON cate.Id = Product.CategoryId WHERE CategoryId != 6 GROUP BY Product.CategoryId")->result();
    if($this->input->server('REQUEST_METHOD') === 'POST')
    { 
        $prod_arr = $this->input->post('langOptgroup');
        $weekday = $this->input->post('week_name');
        $maincategory = $this->input->post('category');
       // $amount = $this->input->post('amount');
        $breakfst = $lunch = $dinner = 0;
        if($maincategory==1){ $breakfst = 1; }elseif($maincategory==2){ $lunch = 1;}else{ $dinner = 1;}
        $inserarr = array();
        
        if(!empty($prod_arr))
        {
        	$implod = implode(',',$prod_arr);
        	$selected = $this->db->query("SELECT ProductId FROM TiffenProductSchedule WHERE WeekDay = '$weekday' AND IsInBreakFast = '$breakfst' AND IsInLunch = '$lunch' AND IsInDinner = '$dinner' AND ProductId IN (".$implod.")")->result();
        	if(!empty($selected))
        	{
        		foreach ($selected as $value) {
        			$sel[] = $value->ProductId;
        		}
        		$kk = array_merge(array_diff($prod_arr, $sel), array_diff($sel, $prod_arr));
            	//$kk = array_diff($kk, $id);
        	}else
        	{
        		$kk = $prod_arr;
        	}
        	foreach ($kk as $key) {
              	$inserarr[] = "('','$weekday','".$key."',$breakfst,$lunch,$dinner,1,'".date('Y-m-d H:i:s')."')";
            }

            $arr_imp = implode(',', $inserarr);
            $insertdata = $this->db->query("INSERT INTO TiffenProductSchedule VALUES ".$arr_imp."");
            if($insertdata)
            {
              	$this->session->set_flashdata('weekd', $weekday);
                $this->session->set_flashdata('cate', $maincategory);
                $this->session->set_flashdata('success', 'Menu successfully Added.');
              	redirect(base_url().'menu/manage_menu');
            }
        }  
    }

    $this->load->view('admin/menu/manage_menu',$data);
  }

  public function subscription()
  { 
     //$data['subscription'] = $this->db->query("SELECT * FROM SubscriptionPlan WHERE Status = 1")->result();
    $data['category'] = $this->db->query("SELECT CategoryName,Id FROM `ProductCategory` ORDER BY Sequence ASC")->result();
     if($this->input->server('REQUEST_METHOD') === 'POST')
    { 
        $data1['Name'] = $this->input->post('name');
        $data1['Price'] = $this->input->post('price');
        $data1['ValidTillMonthCount'] = $this->input->post('month_count');
        $data1['MainCategory'] = $this->input->post('category');
        $prod_arr = $this->input->post('langOpt3');
        $qty = $this->input->post('qty');
        $inserarr = array();
        if(!empty($prod_arr))
        {
            $impprod = implode(',', $prod_arr);
            $impqty = implode(',', $qty);
            $data1['ProductCategory']= $impprod;
            $data1['ProductCategoryQty']= $impqty;
            $data1['CreateOn']= date('Y-m-d H:i:s');
            $data1['Status']= 1;
            //$inserarr = "('','$name','','$maincategory','$subsprice',$validity_count,'$impprod','".date('Y-m-d H:i:s')."',1)";
           	//$insertdata = $this->db->query("INSERT INTO TiffenProductSchedule VALUES ".$inserarr."");
           	$insertdata = $this->common_model->common_insert("SubscriptionPlan",$data1);
            if($insertdata)
            {
              $this->session->set_flashdata('success', 'Subscription successfully Added.');
              redirect(base_url().'menu/subscription');
            }
        }  
    }

     $this->load->view('admin/subscription/subscription',$data);
  }



  public function remove_product_image($image_id =false)
  {
  	  $delete = $this->common_model->deleteData('ProductImages',array('id'=>$image_id));

  	  if($delete)
  	  {
  	  	 echo '1000';exit;
  	  }	
  }

}