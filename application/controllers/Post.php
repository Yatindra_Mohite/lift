<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Post extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
	}
	
	public function user_post()
	{
		$data['post_data'] = $this->common_model->getData('qalame_user_post',array('post_status'=>1),'post_id','DESC');

		$this->load->view('admin/post/unverified_user_post',$data);

	}

	public function rejected_post()
	{
		$data['post_data'] = $this->common_model->getData('qalame_user_post',array('post_status'=>0),'post_id','DESC');

		$this->load->view('admin/post/user_rejected_post',$data);
	}
  
	//Reject Post
	public function reject_post()
	{
        $post_id = $this->input->post('post_id');
        $delete = $this->db->query("UPDATE `qalame_user_post` SET `post_status` = 0 WHERE `post_id` IN($post_id)");

        if($delete)
        {
        	echo $post_id;exit;
        }
	}

	//Delete rejected post
	public function delete_post()
	{
	   $post_id = $this->input->post('post_id');

	   $delete = $this->db->query("DELETE FROM `qalame_user_post` WHERE `post_id` IN($post_id)");

	   if($delete)
	   {
	   	  echo $post_id;exit;
	   }	
	   
	}
	//Reject Post
	public function active_post()
	{
        $post_id = $this->input->post('post_id');

        $active = $this->db->query("UPDATE `qalame_user_post` SET `post_status` = 1 WHERE `post_id` IN($post_id)");

        if($active)
        {
        	echo $post_id;exit;
        }
	}

	public function liked_user($post_id = false)
	{
	  $userdata = array();	
	  $data = '';	
      $data['liked_user'] = $this->common_model->getData('qalame_like_unlike_post',array('post_id'=>$post_id),'like_id','DESC');

      if(!empty($data['liked_user']))
      {
      	  foreach($data['liked_user'] as $userdata1)
      	  {
      	  	 $userdata[] = $this->db->query("SELECT user_id,user_name,user_email,user_image,user_gender,user_location FROM qalame_user WHERE user_id = '".$userdata1->user_id."'")->row();
      	  }	
      }	

      $data['user_data'] = $userdata;

      $this->load->view('admin/post/liked_user',$data);

	}

	public function commented_user($post_id = false)
	{
	  
      $data['user_data'] = $this->common_model->getDataField('qalame_user_post_comment.comment,qalame_user.user_id,qalame_user.user_name,qalame_user.user_email,qalame_user.user_image,qalame_user.user_gender,qalame_user.user_location','qalame_user_post_comment',array('post_id'=>$post_id),'comment_id','DESC',array('qalame_user'=>'qalame_user.user_id = qalame_user_post_comment.user_id'));

      
      $this->load->view('admin/post/user_post_comment',$data);

	}


		
}
