<?php
//phpinfo();exit;
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lift_api extends MY_Controller {
function __construct() {
		parent::__construct();
        date_default_timezone_set("Europe/London");
		
		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
		define('path', base_url().'uploads/');

		/*if($this->check_authentication() != 'success')
        die;*/
	}

	// function testingapi()
	// {
		
 //    	$final_output['status'] = 'success';
 //    	$final_output['message'] = signup_6;
 //    	$final_output['data'] = '9754743271';
	    
	//     header("content-type: application/json");
	//     echo json_encode($final_output); exit;
	// }

	function signup()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
	    	if($json_array->user_mobile !='' && $json_array->user_password!='')
	    	{
	    		if($json_array->user_id!=0)
	    		{
	    			$condition = array('user_id'=>$json_array->user_id);
	    		}else
	    		{
	    			$condition = array('user_mobile'=>$json_array->user_mobile);
	    		}
	    		$seleuser = $this->common_model->common_getRow('user',$condition);
	    		if(!empty($seleuser))
	    		{
	    			if($seleuser->admin_status==1)
	    			{
		    			if($seleuser->user_mobile_status==1)
		    			{	
							$final_output['status'] = 'failed';
	    					$final_output['message'] = constant("signup_1");
		    			}else
		    			{
	    					$otp = '1234';//$this->common_model->random_number();
                			//$this->send_otp($code.$json_array->user_email,$otp);
    						$checkdevice = $this->db->select('user_id')->get_where("user",array('registered_device_id'=>$json_array->user_device_id,'registered_device_id !='=>'ZY223HXKTT'))->row();
    						if(empty($checkdevice))
    						{
		    					if($json_array->user_id!=0)
		    					{
		    						$selectmobile = $this->db->select('user_id')->get_where('user',array('user_mobile'=>$json_array->user_mobile,'user_mobile_status'=>1))->row();
		    						if(empty($selectmobile))
		    						{
		    							unset($json_array->user_id);
		    							$this->common_model->updateData("user",$json_array,$condition);
		    						}else
		    						{
		    							$final_output['status'] = 'failed';
			    						$final_output['message'] = constant("signup_1");
		    							header("content-type: application/json");
	    								echo json_encode($final_output); exit;
		    						}
		    					}else
		    					{
		    						unset($json_array->user_id);
		    						$this->common_model->updateData("user",$json_array,array('user_id'=>$seleuser->user_id));
		    					}
			    				$final_output['status'] = 'success';
		    					$final_output['message'] = constant("signup_2");
			    				$final_output['data'] = $seleuser->user_id; 
    						}else
    						{
    							$final_output['status'] = 'failed'; 
								$final_output['message'] = constant("signup_4");
    						}
		    			}
		    		}else
		    		{
		    			$final_output['status'] = 'failed';
	    				$final_output['message'] = constant("admin_status");
		    		}
	    		}else
	    		{
    				$checkdevice = $this->db->select('user_id')->get_where("user",array('registered_device_id'=>$json_array->user_device_id,'registered_device_id !='=>'ZY223HXKTT'))->row();
    				if(empty($checkdevice))
    				{
	    				$otp = '1234';//$this->common_model->random_number();
	    				$condition = 'true';
	    				$v_code = strtoupper($this->common_model->randomuniqueCode());
	    				$json_array->user_refer_code = $v_code;
	    				if($json_array->user_referal_code != '')
	    				{
	    					$checkreferal = $this->db->select('user_id')->get_where("user",array('user_refer_code'=>$json_array->user_referal_code))->row();
	    					if(!empty($checkreferal))
	    					{
	    						$update_wallet = $this->db->query("UPDATE user SET user_wallet = user_wallet+10 WHERE user_id = ".$checkreferal->user_id."");
	    					}else
	    					{
	    						$condition = 'failed';
	    					}
	    				}
	    				if($condition == 'true')
	    				{
		    				$json_array->mobile_code = $otp;
		    				$json_array->create_date = datetime;
		    				$json_array->user_wallet = 10;
		    				$json_array->admin_status = 1;
		    				//$json_array->registered_device_id = $json_array->user_device_id;
		    				$insert = $this->common_model->common_insert("user",$json_array);
			    			if(!empty($insert) && $insert != false)
			    			{
								//$this->send_otp($code.$json_array->user_email,$v_Code);
		                    	//Send OTP
		                       	$final_output['status'] = 'success';
			    				$final_output['message'] = constant("signup_2");
			    				$final_output['data'] = $insert; 
			    			}else
			    			{
			    				$final_output['status'] = 'failed'; 
							 	$final_output['message'] = some_error;
			    			}
	    				}else
	    				{
	    					$final_output['status'] = 'failed'; 
							$final_output['message'] = constant("signup_3");
	    				}
    				}else
    				{
    					$final_output['status'] = 'failed'; 
						$final_output['message'] = constant("signup_4");
    				}
	    		}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("signup_5");
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = signup_6;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	function otp_verification()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
	    	$user_id = $json_array->user_id;
	    	$otpcode = $json_array->otp;
	    	$data['user_device_id'] = $json_array->user_device_id;
	    	$data['user_device_type'] = $json_array->user_device_type;
	    	$data['user_device_token'] = $json_array->user_device_token;
	    	$data['registered_device_id'] = $json_array->user_device_id;
	    	if(!empty($otpcode) && strlen($otpcode)==4)
	    	{
	    		$checkotp = $this->common_model->common_getRow("user",array("user_id"=>$user_id));
	    		if(!empty($checkotp))
	    		{	
	    			if($checkotp->admin_status == 1)
	    			{
		    			if($checkotp->mobile_code == $otpcode)
		    			{	
		    				$data['mobile_code'] = '';
		    				$data['user_mobile_status'] = 1;
		    				$token = bin2hex(openssl_random_pseudo_bytes(20));
	        				$token = $token.militime;
		    				$data['user_token'] = $token;
		    				$update = $this->common_model->updateData("user",$data,array('user_id'=>$checkotp->user_id));
		    				if($update==TRUE)
		    				{
	    						$updatedevicetoken = $this->common_model->updateData('user',array('user_token'=>'','user_device_token'=>''),array('user_id !='=>$checkotp->user_id,'user_device_id'=>$json_array->user_device_id));
		    						
		    					$image = $lic_image = '';
	    						if(!empty($checkotp->user_image))
			    				{
			    					//$image = base_url().'uploads/user_image/'.$checkotp->user_image;
			    					$image = $checkotp->user_image;
			    				}
			    				if(!empty($seleuserdata->license_image))
			    				{
			    					$lic_image = base_url().'uploads/user_image/license_image/'.$seleuserdata->license_image;
			    				}
				    				$object = array(
										'user_id'=>$checkotp->user_id,
										'user_name'=>$checkotp->user_name,
										'user_image'=>$image,
										'user_mobile'=>$checkotp->user_mobile,
										'user_email'=>$checkotp->user_email,
										'user_gender'=>$checkotp->user_gender,
										'user_wallet'=>$checkotp->user_wallet,
										'user_refer_code'=>$checkotp->user_refer_code,
										'user_device_type'=>$json_array->user_device_type,
										'user_device_id'=>$json_array->user_device_id,
										'user_device_token'=>$json_array->user_device_token,
										'user_home_location'=>$checkotp->user_home_location,
										'user_work_location'=>$checkotp->user_work_location,
										'user_other_location'=>$checkotp->user_other_location,
										'user_home_lat'=>$checkotp->user_home_lat,
										'user_home_lng'=>$checkotp->user_home_lng,
										'user_work_lat'=>$checkotp->user_work_lat,
										'user_work_lng'=>$checkotp->user_work_lng,
										'user_other_lat'=>$checkotp->user_other_lat,
										'user_other_lng'=>$checkotp->user_other_lng,
										'user_license_num'=>$checkotp->user_license_num,
										'license_image'=>$lic_image,
										'company_name'=>$checkotp->company_name,
										'corporate_email'=>$checkotp->corporate_email,
										'emergancy_contact'=>$checkotp->emergancy_contact,
										'profession'=>$checkotp->profession,
										'about'=>$checkotp->about,
										'user_token'=>$token,
										);
									$final_output['status'] = 'success';
					    			$final_output['message'] = constant("mobile_verifiy_1");
									$final_output['data'] = $object;
		    				}else
		    				{
		    					$final_output['status'] = 'failed';
			    				$final_output['message'] = constant("some_error");
		    				}
		    			}else
		    			{
		    				$final_output['status'] = 'failed';
		    				$final_output['message'] = constant("mobile_verifiy_2");
		    			}
	    			}else
		    		{
		    			$final_output['status'] = 'failed';
		    			$final_output['message'] = constant("admin_status");
		    		}
	    		}else
	    		{
	    			$final_output['status'] = 'failed';
	    			$final_output['message'] = constant("login_msg_5");
	    		}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = signup_7;		
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = signup_6;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end otp verification (Y)

	function resend_otp()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array->user_mobile))
	    {
	    	$checkotp = $this->common_model->common_getRow("user",array('user_mobile'=>$json_array->user_mobile));
	    	if(!empty($checkotp))
	    	{	
				if($checkotp->admin_status == 1)
				{
					$otp = '1234';//$this->common_model->random_number();
	           		$updateotp = $this->common_model->updateData("user",array('mobile_code'=>$otp),array('user_id'=>$checkotp->user_id));
					if($updateotp!=false)
					{
                    	//$aa = $this->send_otp($code.$json_array->mobile,$otp);
						//Send OTP function
						$final_output['status'] = 'success';
						$final_output['message'] = constant("mobile_otp_1");
					}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("some_error");
					}
		    	}else
		    	{
		    		$final_output['status'] = 'failed';
		    		$final_output['message'] = constant("admin_status");	
		    	}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("login_msg_5");
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = signup_6;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	//end resend otp 17-11-17

	function login()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
	    	if($json_array->user_mobile !='' && $json_array->user_password!='')
	    	{
	    		$seleuser = $this->common_model->common_getRow('user',array('user_mobile'=>$json_array->user_mobile));
	    		if(!empty($seleuser))
	    		{
	    			if($seleuser->admin_status==1)
	    			{
		    			if($seleuser->user_mobile_status==1)
		    			{	
		    				if($seleuser->user_password == ($json_array->user_password))
			    			{
	    						$token = bin2hex(openssl_random_pseudo_bytes(20));
        						$token = $token.militime;
	    						$update = $this->common_model->updateData('user',array('user_device_type'=>$json_array->user_device_type,'user_device_id'=>$json_array->user_device_id,'user_device_token'=>$json_array->user_device_token,'user_token'=>$token),array('user_id'=>$seleuser->user_id));
	    						
	    						$updatedevicetoken = $this->common_model->updateData('user',array('user_token'=>'','user_device_token'=>''),array('user_id !='=>$seleuser->user_id,'user_device_id'=>$json_array->user_device_id));
	    						
	    						$seleuserdata = $this->common_model->common_getRow('user',array('user_id'=>$seleuser->user_id));
	    						$image = $lic_image = '';
	    						if(!empty($seleuserdata->user_image))
			    				{
			    					//$image = base_url().'uploads/user_image/'.$seleuserdata->user_image;
			    					$image = $checkotp->user_image;
			    				}
			    				if(!empty($seleuserdata->license_image))
			    				{
			    					$lic_image = base_url().'uploads/user_image/license_image/'.$seleuserdata->license_image;
			    				}
								$object = array(
									'user_id'=>$seleuserdata->user_id,
									'user_name'=>$seleuserdata->user_name,
									'user_image'=>$image,
									'user_email'=>$seleuserdata->user_email,
									'user_mobile'=>$seleuserdata->user_mobile,
									'user_gender'=>$seleuserdata->user_gender,
									'user_wallet'=>$seleuserdata->user_wallet,
									'user_refer_code'=>$seleuserdata->user_refer_code,
									'user_device_type'=>$seleuserdata->user_device_type,
									'user_device_id'=>$seleuserdata->user_device_id,
									'user_device_token'=>$seleuserdata->user_device_token,
									'user_home_location'=>$seleuserdata->user_home_location,
									'user_work_location'=>$seleuserdata->user_work_location,
									'user_other_location'=>$seleuserdata->user_other_location,
									'user_home_lat'=>$seleuserdata->user_home_lat,
									'user_home_lng'=>$seleuserdata->user_home_lng,
									'user_work_lat'=>$seleuserdata->user_work_lat,
									'user_work_lng'=>$seleuserdata->user_work_lng,
									'user_other_lat'=>$seleuserdata->user_other_lat,
									'user_other_lng'=>$seleuserdata->user_other_lng,
									'user_license_num'=>$seleuserdata->user_license_num,
									'license_image'=>$lic_image,
									'company_name'=>$seleuserdata->company_name,
									'corporate_email'=>$seleuserdata->corporate_email,
									'emergancy_contact'=>$seleuserdata->emergancy_contact,
									'profession'=>$seleuserdata->profession,
									'about'=>$seleuserdata->about,
									'user_token'=>$token
									);
								$final_output['status'] = 'success';
	    						$final_output['message'] = constant("login_msg_1");
	    						$final_output['data'] = $object;
			    			}else
			    			{
			    				//credentials does not matched
			    				$final_output['status'] = 'failed';
	    						$final_output['message'] = constant("login_msg_2");
	    					}
		    			}else
		    			{
		    		       	$otp = '1234';//$this->common_model->random_number();
							///$this->send_otp($code.$json_array->user_email,$otp);
	    					$this->common_model->updateData("user",array('mobile_code'=>$otp),array('user_id'=>$seleuser->user_id));
		    				$final_output['status'] = 'unverified_user';
	    					$final_output['message'] = constant("mobile_otp_1");
		    				$final_output['data'] = $seleuser->user_id;
		    			}
	    			}else
	    			{
						$final_output['status'] = 'failed';
	    				$final_output['message'] = constant("admin_status");
	    			}
	    		}else
	    		{
	    			$final_output['status'] = 'failed';
	    			$final_output['message'] = constant("login_msg_5");
	    		}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("signup_5");
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = signup_6;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end login (Y)

	function social_signup()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array->facebook_id))
	    {
	    	$checkotp = $this->common_model->common_getRow("user",array('facebook_id'=>$json_array->facebook_id));
	    	if(!empty($checkotp))
	    	{	
				if($checkotp->admin_status == 1)
				{
					if($checkotp->user_mobile_status == 1)
		           	{
		           		$token = bin2hex(openssl_random_pseudo_bytes(20));
        				$token = $token.militime;
		           		$image = $lic_image ='';
						if(!empty($checkotp->user_image))
	    				{
	    					//$image = base_url().'uploads/user_image/'.$seleuserdata->user_image;
	    					$image = $checkotp->user_image;
	    				}
	    				if(!empty($seleuserdata->license_image))
	    				{
	    					$lic_image = base_url().'uploads/user_image/license_image/'.$seleuserdata->license_image;
	    				}
		           		$data = 1;
						$msg = constant("login_msg_1");
		           		$object = array(
								'user_id'=>$checkotp->user_id,
								'user_name'=>$checkotp->user_name,
								'user_image'=>$image,
								'user_email'=>$checkotp->user_email,
								'user_mobile'=>$checkotp->user_mobile,
								'user_gender'=>$checkotp->user_gender,
								'user_device_type'=>$json_array->user_device_type,
								'user_device_id'=>$json_array->user_device_id,
								'user_device_token'=>$json_array->user_device_token,
								'user_home_location'=>$checkotp->user_home_location,
								'user_work_location'=>$checkotp->user_work_location,
								'user_other_location'=>$checkotp->user_other_location,
								'user_home_lat'=>$checkotp->user_home_lat,
								'user_home_lng'=>$checkotp->user_home_lng,
								'user_work_lat'=>$checkotp->user_work_lat,
								'user_work_lng'=>$checkotp->user_work_lng,
								'user_other_lat'=>$checkotp->user_other_lat,
								'user_other_lng'=>$checkotp->user_other_lng,
								'user_license_num'=>$checkotp->user_license_num,
								'license_image'=>$lic_image,
								'company_name'=>$checkotp->company_name,
								'corporate_email'=>$checkotp->corporate_email,
								'emergancy_contact'=>$checkotp->emergancy_contact,
								'profession'=>$checkotp->profession,
								'about'=>$checkotp->about,
								'user_token'=>$token,
								);
		           	}
		           	elseif($checkotp->user_mobile == '') 
		           	{
		           		$data = 2;
						$msg = 'Successfull';
						$object = array('user_id'=>$checkotp->user_id);
		           	}elseif($checkotp->user_mobile_status == 0)
		           	{
		           		$otp ='1234';
		           		$updateotp = $this->common_model->updateData("user",array('mobile_code'=>$otp),array('user_id'=>$checkotp->user_id));
						//$aa = $this->send_otp($code.$json_array->mobile,$otp);
		           		$data = 3;
						$msg = constant("signup_2");
						$object = array('user_id'=>$checkotp->user_id,'user_mobile'=>$checkotp->user_mobile);
		           	}	
		           	$final_output['status'] = 'success';
					$final_output['signup_status'] = $data;
					$final_output['message'] = $msg;
					$final_output['data'] = $object;
				}else
		    	{
		    		$final_output['status'] = 'failed';
		    		$final_output['message'] = constant("admin_status");	
		    	}
	    	}else
	    	{
	    		$checkdevice = $this->db->select('user_id')->get_where("user",array('registered_device_id'=>$json_array->user_device_id,'registered_device_id !='=>'ZY223HXKTT'))->row();
				if(empty($checkdevice))
				{
					$json_array->registered_device_id = $json_array->user_device_id;
					$json_array->admin_status = 1;
					$json_array->create_date = datetime;
					$insert = $this->common_model->common_insert("user",$json_array);
					if(!empty($insert))
					{
						$final_output['status'] = 'success';
						$final_output['signup_status'] = 2;
						$final_output['message'] = 'Successfull';
						$final_output['data'] = array('user_id'=>$insert);
					}else
					{
						$final_output['status'] = 'failed';
		    			$final_output['message'] = some_error;
					}
				}else
				{
					$final_output['status'] = 'failed';
		    		$final_output['message'] = constant("signup_4");
				}
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = signup_6;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	//Social signin(Y)

	function forgot_password()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
		$type = 1; //email

	    if(!empty($json_array))
	    {
	    	if($json_array->user_mobile!='')
	    	{
	    		$seleuser = $this->common_model->common_getRow('user',array('user_mobile'=>$json_array->user_mobile));
	    		if(!empty($seleuser))
	    		{
	    			if($seleuser->admin_status==1)
	    			{
    					$otp = '1234';
	    				//$msg = "Your OTP for change password is: ".$otp;
						//Send OTP
		           		//$otp = $this->common_model->random_number();
						//$this->send_otp($code.$json_array->user_email,$otp);
		           		$update = $this->common_model->updateData("user",array("mobile_code"=>$otp),array('user_id'=>$seleuser->user_id));
    					
		           		$final_output['status'] = 'success';
						$final_output['message'] = constant("mobile_otp_1");
		           		$final_output['data'] = $seleuser->user_id;

		    		}else
	    			{
						$final_output['status'] = 'failed';
	    				$final_output['message'] = constant("admin_status");
	    			}
	    		}else
	    		{
	    			$final_output['status'] = 'failed';
	    			$final_output['message'] = constant("login_msg_5");
	    		}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = signup_6;
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = signup_6;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);	
	}
	//end forgot password (Y)

	function change_password_with_otp()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
	    	$userid = $json_array->user_id;
	    	$otpcode = $json_array->otp;
	    	$password = $json_array->user_password;
	    	if(!empty($otpcode) && !empty($userid))
	    	{ 
	    		$checkotp = $this->common_model->common_getRow("user",array("user_id"=>$userid,"admin_status"=>1));
	    		if(!empty($checkotp))
	    		{	
	    			if($checkotp->mobile_code == $otpcode)
	    			{
		    			$data['mobile_code'] = '';
	    				$data['user_password'] = $password;
	    				$data['user_mobile_status'] = 1;
	    				$update = $this->common_model->updateData("user",$data,array('user_id'=>$checkotp->user_id));
	    				if($update==TRUE)
	    				{	
	    					$image = '';
							if(!empty($checkotp->user_image))
		    				{
		    					//$image = base_url().'uploads/user_image/'.$seleuserdata->user_image;
		    					$image = $checkotp->user_image;
		    				}
	    					/*$object = array(
								'user_id'=>$checkotp->user_id,
								'user_name'=>$checkotp->user_name,
								'user_image'=>$image,
								'user_email'=>$checkotp->user_email,
								'user_mobile'=>$checkotp->user_mobile,
								'user_gender'=>$checkotp->user_gender,
								'user_device_type'=>$checkotp->user_device_type,
								'user_device_id'=>$checkotp->user_device_id,
								'user_device_token'=>$checkotp->user_device_token,
								//'user_token'=>$token,
								);*/
	    					$final_output['status'] = 'success';
			    			$final_output['message'] = constant("changed_1");
							//$final_output['data'] = $object;
	    				}else
	    				{
	    					$final_output['status'] = 'failed';
		    				$final_output['message'] = constant("some_error");
	    				}
	    			}else
	    			{
	    				$final_output['status'] = 'failed';
	    				$final_output['message'] = constant("mobile_verifiy_2");
	    			}
	    		}else
	    		{
	    			$final_output['status'] = 'failed';
	    			$final_output['message'] = constant("admin_status");
	    		}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = signup_7;		
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = signup_6;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end send_password_mobile (Y)

	function change_password()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			if(!empty($json_array))
			{
				if($aa['data']->admin_status ==1){
				    $final_output = array();
				    if(!empty($json_array->current_password) && !empty($json_array->new_password))
				    {
				    	$selepas = $this->db->select('user_password')->get_where("user",array('user_id'=>$aa['data']->user_id,'user_password'=>$json_array->current_password))->row();
				    	if(!empty($selepas))
				    	{
					    	$updatepass= $this->common_model->updateData("user",array('user_password'=>$json_array->new_password),array('user_id'=>$aa['data']->user_id));
					    	if($updatepass==true)
					    	{	
					    		$final_output['status'] = 'success';
		    					$final_output['message'] = constant("changed_1");
					    	}else
					    	{
					    		$final_output['status'] = 'failed';
		    					$final_output['message'] = constant("some_error");
					    	}
				    	}else
				    	{
				    		$final_output['status'] = 'failed';
							$final_output['message'] = constant("changed_3");
				    	}
				    }else
				    {
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("changed_2");
				    }
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = signup_6;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end Chagne Password (Y)

	function add_vehicle()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			if(!empty($json_array))
			{
				if($aa['data']->admin_status ==1){
				    $final_output = array();
				    $v_image = $json_array->vehicle_image;
				    $v_rc_image = $json_array->vehicle_registration_image;
				    $user_id = $aa['data']->user_id;
				    //vehicle_type = 1 for bike , 2 for four car
				    if(!empty($json_array->vehicle_registration_num) && !empty($json_array->vehicle_model))
				    {
						$vimages = $rcimages = '';
						    if (!empty($v_image)) { 
		                        for($i=0; $i<count($v_image); $i++)
		                        { 
		                            $image_name = 'vimg'.$i.'_'.militime.$user_id.'.jpeg';
		                            //exit;
		                            $path = path.'vehicle_image/'.$image_name;
		                          
		                            $base64img = str_replace('data:image/jpeg;base64,', '', $v_image[$i]);
		                          
		                            $data1 = base64_decode($base64img);
		                          
		                            $aa = file_put_contents($path, $data1);
		                        }
		                        $json_array->vehicle_image = $image_name;
		                        $vimages = $path;
		                    }
		                    if (!empty($v_rc_image)) { 
		                        for($i=0; $i<count($v_rc_image); $i++)
		                        { 
		                            $image_name1 = 'vrcimg'.$i.'_'.militime.$user_id.'.jpeg';
		                            //exit;
		                            $path1 = path.'vehicle_image/'.$image_name1;
		                          
		                            $base64img1 = str_replace('data:image/jpeg;base64,', '', $v_rc_image[$i]);
		                          
		                            $data1 = base64_decode($base64img1);
		                          
		                            $aa = file_put_contents($path1, $data1);
		                        }
		                        $json_array->vehicle_registration_image = $image_name1;
		                    	$rcimages = $path;
		                    }
		                    $json_array->user_id = $user_id;
		                    $json_array->create_date = datetime;
				        	$updatepass= $this->common_model->common_insert("vehicle",$json_array);
					    	if($updatepass==true)
					    	{	
					    		$final_output['status'] = 'success';
		    					$final_output['message'] = constant("vehicle_add");
		    					$final_output['data'] = array('vehicle_image'=>$vimages,'vehicle_registration_image'=>$rcimages,'vehicle_registration_num'=>$json_array->vehicle_registration_num,'vehicle_model'=>$json_array->vehicle_model);
					    	}else
					    	{
					    		$final_output['status'] = 'failed';
		    					$final_output['message'] = constant("some_error");
					    	}
				    }else
				    {
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("changed_2");
				    }
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = signup_6;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end Chagne Password (Y)

	function edit_vehicle()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			if(!empty($json_array))
			{
				if($aa['data']->admin_status ==1){
				    $final_output = array();
				    $v_image = $json_array->vehicle_image;
				    $v_rc_image = $json_array->vehicle_registration_image;
				    $vehicle_id = $json_array->vehicle_id;
				    
				    $user_id = $aa['data']->user_id;
				    //vehicle_type = 1 for bike , 2 for four car
				    if(!empty($json_array->vehicle_registration_num) && !empty($json_array->vehicle_model))
				    {
					 	$selevehicle = $this->db->select('vehicle_id,vehicle_image,vehicle_registration_image')->get_where("vehicle",array("vehicle_id"=>$vehicle_id))->row();
					    if(!empty($selevehicle))
					    {
						    $vimages = $rcimages = '';
						    if (!empty($v_image)) { 
		                        for($i=0; $i<count($v_image); $i++)
		                        { 
		                            $image_name = 'vimg'.$i.'_'.militime.$user_id.'.jpeg';
		                            //exit;
		                            $path = path.'vehicle_image/'.$image_name;
		                          
		                            $base64img = str_replace('data:image/jpeg;base64,', '', $v_image[$i]);
		                          
		                            $data1 = base64_decode($base64img);
		                          
		                            $aa = file_put_contents($path, $data1);
		                        }
		                        $json_array->vehicle_image = $image_name;
		                        $vimages = base_url().'uploads/vehicle_image/'.$image_name;
		                    }else
		                    {
		                    	if(!empty($selevehicle->vehicle_image))
		                    	{
		                    		$vimages = base_url().'uploads/vehicle_image/'.$selevehicle->vehicle_image;
		                    	}
		                    	unset($json_array->vehicle_image);
		                    }
		                    
		                    if (!empty($v_rc_image)) { 
		                        for($i=0; $i<count($v_rc_image); $i++)
		                        { 
		                            $image_name1 = 'vrcimg'.$i.'_'.militime.$user_id.'.jpeg';
		                            //exit;
		                            $path1 = path.'vehicle_image/'.$image_name1;
		                          
		                            $base64img1 = str_replace('data:image/jpeg;base64,', '', $v_rc_image[$i]);
		                          
		                            $data1 = base64_decode($base64img1);
		                          
		                            $aa = file_put_contents($path1, $data1);
		                        }
		                        $json_array->vehicle_registration_image = $image_name1;
		                   		$rcimages = base_url().'uploads/vehicle_image/'.$image_name1;
		                    }else
		                    {
		                    	if(!empty($selevehicle->vehicle_registration_image))
		                    	{
		                    		$rcimages = base_url().'uploads/vehicle_image/'.$selevehicle->vehicle_registration_image;
		                    	}
		                    	unset($json_array->vehicle_registration_image);
		                    }
				        	$updatepass= $this->common_model->updateData("vehicle",$json_array,array('vehicle_id'=>$vehicle_id));
					    	if($updatepass==true)
					    	{	
					    		$final_output['status'] = 'success';
		    					$final_output['message'] = constant("vehicle_update");
		    					$final_output['data'] = array('vehicle_image'=>$vimages,'vehicle_registration_image'=>$rcimages,'vehicle_registration_num'=>$json_array->vehicle_registration_num,'vehicle_model'=>$json_array->vehicle_model,'vehicle_type'=>$json_array->vehicle_type);
					    	}else
					    	{
					    		$final_output['status'] = 'failed';
		    					$final_output['message'] = constant("some_error");
					    	}
					    }else
					    {
					    	$final_output['status'] = 'failed';
							$final_output['message'] = constant("vehicle_id");
					    }
				    }else
				    {
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("changed_2");
				    }
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = signup_6;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end Chagne Password (Y)

	function my_vehicle_list()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			if($aa['data']->admin_status ==1)
			{
				$final_output = array();
				$user_id = $aa['data']->user_id;
				$arr = array();
				$selevehicle = $this->common_model->getData("vehicle",array('user_id'=>$user_id));
				if(!empty($selevehicle))
				{
					foreach ($selevehicle as $key) {
						$vimage = $rcimage = '';
						if(!empty($key->vehicle_image))
						{
							$vimage = path.'vehicle_image/'.$key->vehicle_image;
						}
						if(!empty($key->vehicle_image))
						{
							$rcimage = path.'vehicle_image/'.$key->vehicle_registration_image;
						}
						$arr[] = array(
								'vehicle_id'=>$key->vehicle_id,
								'vehicle_model'=>$key->vehicle_model,
								'vehicle_registration_num'=>$key->vehicle_registration_num,
								'vehicle_image'=>$vimage,
								'vehicle_registration_image'=>$rcimage
							);
					}
				}
				if(!empty($arr))
				{
					$final_output['status'] = 'success';
					$final_output['message'] = constant("vehicle_list");
					$final_output['data'] = $arr;
				}else
				{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("vehicle_list_2");
				}
			}else
			{
				$final_output['status'] = 'failed';
				$final_output['message'] = constant("admin_status");
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end vehicle list

	function vehicle_delete()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			if(!empty($json_array))
			{
				if($aa['data']->admin_status ==1){
				    $final_output = array();
				    $vehicle_id = $json_array->vehicle_id;
				    $user_id = $aa['data']->user_id;
				    //vehicle_type = 1 for bike , 2 for four car
				    if(!empty($vehicle_id))
				    {
					 	$selevehicle = $this->db->select('vehicle_id')->get_where("vehicle",array("vehicle_id"=>$vehicle_id))->row();
					    if(!empty($selevehicle))
					    {
						    $updatepass= $this->common_model->deleteData("vehicle",array('vehicle_id'=>$vehicle_id));
					    	if($updatepass!=false)
					    	{	
					    		$final_output['status'] = 'success';
		    					$final_output['message'] = constant("vehicle_delete");
		    				}else
					    	{
					    		$final_output['status'] = 'failed';
		    					$final_output['message'] = constant("some_error");
					    	}
					    }else
					    {
					    	$final_output['status'] = 'failed';
							$final_output['message'] = constant("vehicle_id");
					    }
				    }else
				    {
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("changed_2");
				    }
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = signup_6;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end Chagne Password (Y)

	function update_profile()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			if($aa['data']->admin_status==1)
			{
				$json_array = file_get_contents('php://input');
			    $json = json_decode($json_array);
				if(!empty($json))
				{
					$user_id = $aa['data']->user_id;
					$data['user_name'] = $json->user_name;
					$data['user_home_location'] = $json->user_home_location;
					$data['user_work_location'] = $json->user_work_location;
					$data['user_other_location'] = $json->user_other_location;
					$data['user_license_num'] = $json->user_license_num;
					$data['user_home_lat'] = $json->user_home_lat;
					$data['user_home_lng'] = $json->user_home_lng;
					$data['user_work_lat'] = $json->user_work_lat;
					$data['user_work_lng'] = $json->user_work_lng;
					$data['user_other_lat'] = $json->user_other_lat;
					$data['user_other_lng'] = $json->user_other_lng;
					$data['company_name'] = $json->company_name;
					$data['corporate_email'] = $json->corporate_email;
					$data['user_gender'] = $json->user_gender;
					//$data['emergancy_contact'] = $json->emergancy_contact;
					$data['profession'] = $json->profession;
					$data['about'] = $json->about;
					$userimage  = $json->user_image;
					$license_image  = $json->license_image;
					if (!empty($userimage)) { 
			                      
	                    $image_name1 = 'user_image_'.$user_id.'.jpeg';
	                    //exit;
	                    $path1 = path.'user_image/'.$image_name1;
	                  
	                    $base64img1 = str_replace('data:image/jpeg;base64,', '', $userimage);
	                  
	                    $data1 = base64_decode($base64img1);
	                  
	                    $aa = file_put_contents($path1, $data1);
	                    
	                    $data['user_image'] = $image_name1;
	                	$image = $path1;
			        }else
					{
						if($aa['data']->user_image !=''){
							$image = path.'user_image/'.$aa['data']->user_image;
						}else{
							$image = '';
						}
					}

					if (!empty($license_image)) { 
			                      
	                    $image_name1 = 'user_lic_image_'.$user_id.'.jpeg';
	                    //exit;
	                    $path1 = path.'user_image/license_image/'.$image_name1;
	                  
	                    $base64img1 = str_replace('data:image/jpeg;base64,', '', $license_image);
	                  
	                    $data1 = base64_decode($base64img1);
	                  
	                    $aa = file_put_contents($path1, $data1);
	                    
	                	$lic_image = $path1;
	                	$data['license_image'] = $image_name1;
			        }else
					{
						if($aa['data']->license_image !=''){
							$lic_image = path.'user_image/license_image/'.$aa['data']->license_image;
						}else{
							$lic_image = '';
						}
					}
					//$data['update_date'] = date('Y-m-d H:i:s');
					$update_data = $this->common_model->updateData("user",$data,array('user_id'=>$user_id));
					if($this->db->affected_rows())
					{
						
						$data['user_image'] = $image;
						$data['license_image'] = $lic_image;
						$final_output['status'] = 'success'; 
					 	$final_output['message'] = constant("update_profile_2");
						$final_output['data'] = $data; 
					}else
					{
						$final_output['status'] = 'failed'; 
					 	$final_output['message'] = constant("some_error");
					}
				}else
				{
					$final_output['status'] = 'failed'; 
					$final_output['message'] = signup_6;
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("admin_status");
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end update profile (Y)

	function rating_and_review()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			if(!empty($json_array))
			{
				if($aa['data']->admin_status ==1){
				    $final_output = array();
				    $booking_id = $json_array->booking_id;
				    $rating = $json_array->rating;
				    $review = $json_array->review;
				    $user_id = $aa['data']->user_id;
				    //vehicle_type = 1 for bike , 2 for four car
				    if(!empty($booking_id))
				    {
						// $selbooking = $this->db->select('booking_id')->get_where("booking",array('booking_id'=>$booking_id))->row();
						// if(!empty($selbooking))
						// {
						    	$selrating = $this->db->select('id')->get_where("rating_&_review",array('booking_id'=>$booking_id,'user_id'=>$user_id))->row();
								if(!empty($selrating))
								{
						        	$updatepass= $this->common_model->updateData("rating_&_review",array('rating'=>$rating,'review'=>$review),array('id'=>$selrating->id));
								}else
								{
							    	$json_array->create_date = datetime;
							    	$json_array->user_id = $user_id;
						        	$updatepass= $this->common_model->common_insert("rating_&_review",$json_array);
								}
						    	if($updatepass==true)
						    	{	
						    		$final_output['status'] = 'success';
			    					$final_output['message'] = constant("rating_1");
			    				}else
						    	{
						    		$final_output['status'] = 'failed';
			    					$final_output['message'] = constant("some_error");
						    	}
						// }else
						// {
						// 	//booking not found
						// }
				    }else
				    {
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("changed_2");
				    }
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = signup_6;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end RATING review (Y)

	function add_emergency_contact()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			if($aa['data']->admin_status==1)
			{
				$json_array = file_get_contents('php://input');
			    $json = json_decode($json_array);
				if(!empty($json))
				{
					$user_id = $aa['data']->user_id;
					$data['contact_name'] = $json->contact_name;
					$data['contact_num'] = $json->contact_num;
					$contact_image  = $json->contact_image;
					if(!empty($data['contact_num']) && !empty($data['contact_name']))
					{
						$selecontact = $this->db->query("SELECT count(user_id) as maxcount FROM emergency_contact WHERE user_id = '$user_id'")->row();
						if($selecontact->maxcount < 3)
						{
							if (!empty($contact_image)) { 
				                      
			                    $image_name1 = 'contact_image_'.$user_id.'.jpeg';
			                    //exit;
			                    $path1 = path.'contact_image/'.$image_name1;
			                  
			                    $base64img1 = str_replace('data:image/jpeg;base64,', '', $contact_image);
			                  
			                    $data1 = base64_decode($base64img1);
			                  
			                    $aa = file_put_contents($path1, $data1);
			                    
			                    $data['contact_image'] = $image_name1;
			                	$image = $path1;
				        	}
							$data['create_date'] = date('Y-m-d H:i:s');
							$data['user_id'] = $user_id;
							$update_data = $this->common_model->common_insert("emergency_contact",$data);
							if($this->db->affected_rows())
							{
								$final_output['status'] = 'success'; 
							 	$final_output['message'] = constant("contact_2");
							}else
							{
								$final_output['status'] = 'failed'; 
							 	$final_output['message'] = constant("some_error");
							}
						}else
						{
							$final_output['status'] = 'failed'; 
							$final_output['message'] = constant("contact_1");
						}
					}else
					{
						$final_output['status'] = 'failed'; 
						$final_output['message'] = signup_6;
					}
				}else
				{
					$final_output['status'] = 'failed'; 
					$final_output['message'] = signup_6;
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("admin_status");
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end update profile (Y)

	function edit_emergency_contact()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			if($aa['data']->admin_status==1)
			{
				$json_array = file_get_contents('php://input');
			    $json = json_decode($json_array);
				if(!empty($json))
				{
					$user_id = $aa['data']->user_id;
					$data['contact_name'] = $json->contact_name;
					$data['contact_num'] = $json->contact_num;
					$contact_image  = $json->contact_image;
					$contact_id  = $json->contact_id;
					if(!empty($data['contact_num']) && !empty($data['contact_name'])  && !empty($contact_id))
					{
						$selecontact = $this->db->query("SELECT contact_id FROM emergency_contact WHERE user_id = '$user_id' AND contact_id = '$contact_id'")->row();
						if(!empty($selecontact))
						{
							if (!empty($contact_image)) { 
				                      
			                    $image_name1 = 'contact_image_'.$user_id.'.jpeg';
			                    //exit;
			                    $path1 = path.'contact_image/'.$image_name1;
			                  
			                    $base64img1 = str_replace('data:image/jpeg;base64,', '', $contact_image);
			                  
			                    $data1 = base64_decode($base64img1);
			                  
			                    $aa = file_put_contents($path1, $data1);
			                    
			                    $data['contact_image'] = $image_name1;
			                	$image = $path1;
				        	}
							$update_data = $this->common_model->updateData("emergency_contact",$data,array('contact_id'=>$contact_id));
							if($this->db->affected_rows())
							{
								$final_output['status'] = 'success'; 
							 	$final_output['message'] = constant("contact_3");
							}else
							{
								$final_output['status'] = 'failed'; 
							 	$final_output['message'] = constant("some_error");
							}
						}else
						{
							$final_output['status'] = 'failed'; 
							$final_output['message'] = constant("contact_4");
						}
					}else
					{
						$final_output['status'] = 'failed'; 
						$final_output['message'] = signup_6;
					}
				}else
				{
					$final_output['status'] = 'failed'; 
					$final_output['message'] = signup_6;
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("admin_status");
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end edit contact (Y)

	function emergency_contact_list()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			if($aa['data']->admin_status==1)
			{
				$user_id = $aa['data']->user_id;
				$selecontact = $this->db->query("SELECT * FROM emergency_contact WHERE user_id = '$user_id' ORDER BY contact_name ASC")->result();
				if(!empty($selecontact))
				{
					foreach ($selecontact as $key ) {
						$path1='';
						if($key->contact_image!='')
						{
							$path1 = path.'contact_image/'.$key->contact_image;
						}

						$array[] =array(
								'contact_id'=>$key->contact_id,
								'contact_name'=>$key->contact_name,
								'contact_num'=>$key->contact_num,
								'contact_image'=>$path1
								);
					}
					$final_output['status'] = 'success'; 
				 	$final_output['message'] = constant("contact_5");
				 	$final_output['data'] = $array;
				}else
				{
					$final_output['status'] = 'failed'; 
					$final_output['message'] = constant("contact_6");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("admin_status");
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end contact list (Y)

	function emergency_contact_delete()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			if($aa['data']->admin_status==1)
			{
				$json_array = file_get_contents('php://input');
			    $json = json_decode($json_array);
				if(!empty($json))
				{
					$user_id = $aa['data']->user_id;
					$contact_id  = $json->contact_id;
					if(!empty($contact_id))
					{
						$selecontact = $this->db->query("SELECT contact_id FROM emergency_contact WHERE user_id = '$user_id' AND contact_id = '$contact_id'")->row();
						if(!empty($selecontact))
						{
							$update_data = $this->common_model->deleteData("emergency_contact",array('contact_id'=>$contact_id));
							if($this->db->affected_rows())
							{
								$final_output['status'] = 'success'; 
							 	$final_output['message'] = constant("contact_7");
							}else
							{
								$final_output['status'] = 'failed'; 
							 	$final_output['message'] = constant("some_error");
							}
						}else
						{
							$final_output['status'] = 'failed'; 
							$final_output['message'] = constant("contact_4");
						}
					}else
					{
						$final_output['status'] = 'failed'; 
						$final_output['message'] = signup_6;
					}
				}else
				{
					$final_output['status'] = 'failed'; 
					$final_output['message'] = signup_6;
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("admin_status");
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end contact delete (Y)

	function check_version()
	{
		$json = file_get_contents('php://input');
		$json_array = json_decode($json);
		if($json_array->type=='android')
		{
			$check_version = $this->db->select('min_version')->get_where("app_version",array('min_version <=' => $json_array->version_code,'version_id'=>1))->row();
		}else
		{
			$check_version = $this->db->select('min_version')->get_where("app_version",array('min_version <=' => $json_array->version_code,'version_id'=>2))->row();
		}
		if(!empty($check_version))
		{
			$final_output['status'] = 'success';
	    	$final_output['message'] = 'successfully';
		}else
		{
			$final_output['status'] = 'failed';
	    	$final_output['message'] = constant("version_".$json_array->language."_1");
		}
		header("content-type: application/json");
	    echo json_encode($final_output);	
	}
	//end check app version (Y)	

	function check_authentication()
	{
	    $response = '';
	 	$headers = apache_request_headers();
		if(!empty($headers) && isset($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$final_output['data'] = $check['data'];
				$final_output['status'] = "true";
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['status'] ="false";
			$final_output['message'] = "Unauthorised access";
		}
	    return $final_output;	
	}
	
	function ChechAuth($token)
	{
		$auth = $this->common_model->getDataField('user_id,admin_status,user_name,user_image,license_image','user',array('user_token'=>$token));
		if(!empty($auth))
		{
			$abc['status'] = "true";
			$abc['data'] = $auth[0];
			return $abc;
		}else
		{
			$abc['status'] = "false";
			return $abc;
		}
	}
	
}
?>
