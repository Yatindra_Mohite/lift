<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if($userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		
	}
	
	public function index()
	{ 
		if(isset($_POST['submit']))
		{   
			     $username = $this->input->post ( 'username' );
				 $password = $this->input->post('password') ;
				 $AdminData = $this->common_model->common_getRow('qalame_admin',array('email'=>$username,'password'=>md5($password)));
				if (!empty($AdminData)) 
				{
						$this->session->set_userdata ( array (
								'admin_id'   => $AdminData->admin_id,
								//'email'   => $AdminData->email,
								));
							redirect(base_url().'dashboard');
				}
				else
				{
				  $this->session->set_flashdata('msg' ,'Email or Password Not Matched.');	
				  redirect(base_url('login'));	
				}	
		}
		$this->load->view('admin/index');
	}
	
}
