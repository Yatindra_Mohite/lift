<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
define('GOOGLE_API_KEY', 'AAAA1gxSyGc:APA91bG-sIp8WdSnbXk6ZV8S_8utRavdvVdlhKDcVIaYh52y3n9VJ0pzVA8FygULXWYPlO3sfZXykp_mGXXFF3I29-eBK7GRzKTs1RzfdYhh8TcNtIY4BqtWFXYxZquZZARLhPqymHJX');

define('PAYTM_MERCHANT_KEY', 'A4%Q!gOPVsq0RWqB');



//Login
define('login_msg_1', 'Login Successfull.');
define('login_msg_2', 'Invalid login credentials.');
define('login_msg_en_3', 'Please check your email if you have already verification email OR click on resend button for email verification link.');
define('login_msg_5', 'Account does not exists.');


//Common Function
define('admin_status', 'Your lift account has been temporarily suspended as a security precaution.');
define('some_error', 'Something went wrong! please try again later.');
define('no_data_en', 'Data not available.'); 

//Update Profile
define('update_profile_en_1', 'Only jpg, jpeg and png file type is allowed.');
define('update_profile_2', 'Profile successfully updated.');
define('update_profile_en_3', 'Mobile number is already registered.');

//Signup
define('signup_1', 'Mobile number is already registered.');
define('signup_2', 'Registered successfully, please check your inbox for OTP.');
define('signup_3', 'Invalid referal code.');
define('signup_4', 'Device already registered with another mobile number.');
define('signup_5', 'Mobile and Password is required.');
define('signup_6', 'No Request Parameter Found.');
define('signup_7', 'Invalid Request Parameter.');

//resend verification email
define('resent_en_1', 'Verification link has been send to your registered email address.');

//Forgot password
define('forgot_en_1', 'Reset password link has been send to your registered email address.');

//Change Password
define('changed_1', 'Password changed successfully.');
define('changed_2', 'All fields are required.');
define('changed_3', 'Old password does not match.');

//vehicle
define('vehicle_add', 'Vehicle successfully added.');
define('vehicle_add_2', 'Registration number already exists.');
define('vehicle_id', 'Vehicle not found, please add vehicle to offer a ride');
define('vehicle_update', 'Vehicle successfully updated.');
define('vehicle_list', 'Vehicle list available.');
define('vehicle_list_2', 'Vehicle not found, please add vehicle to offer a ride');
define('vehicle_delete', 'Vehicle successfully removed');

//rating
define('rating_1', 'Rating submitted successfully.');
//contact
define('contact_1', "Only 3 emergency contact can be added.");
define('contact_2', 'Contact added successfully.');
define('contact_3', 'Contact edited successfully.');
define('contact_4', 'Unable to find contact.');
define('contact_5', 'Contact list available.');
define('contact_6', 'Contact is not added in contact list.');
define('contact_7', 'Contact successfully removed.');

//
define('support', 'Thanks! soon we will acknowledge your suggestions.');

//App version
define('version_en_1', 'This version no longer supported, Please update the application.');


//OTP verification
define('mobile_verifiy_1', "Mobile number has been verified successfully.");
define('mobile_verifiy_2', "OTP does not match.");
define('mobile_otp_1', "OTP has been sent to your registered mobile number.");

//forgot password otp
define('send_password_en_1', "Password has been changed successfully.");


//offer ride
define('offer_ride_1', "You can not offer another ride at the moment, as you are already offering a ride.");
define('offer_ride_2', "No ride finder available at the moment, we are searching for you and if you want you can choose offer later.");

//finder ride
define('finder_ride_1', "You can not find another ride at the moment, as you have an active ride.");
define('finder_ride_2', "No ride offerer available at the moment, we are searching for you and if you want you can choose ride later.");
define('finder_ride_3', "You can not find ride at the moment because you have insufficent balance in your wallet for this ride.");

//sent request
define('same_user_id', "Unable to sent request.");

//END Verified by uday

//lift id ride 
define('ride_by_lift_id_1', "No ride available for this location.");

//Cancel friend request


//Facebook link with user
define('fb_1', "Facebook id already exists.");
define('fb_2', "Facebook id successfully linked.");

//Favourite location
define('fvlocation_1', "Successfully updated.");
define('fvlocation_2', "Successfully deleted.");
define('failedlocation_1', "Unable to process for update location.");
define('failedlocation_2', "Unable to process for delete location.");
