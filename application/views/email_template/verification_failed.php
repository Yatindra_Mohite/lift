<html>
<head>
	<title>Email Template</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
</head>
<body style="margin: 10%;" style="bg-color:blue;">
	
		<table width="600" style="font-family: 'Roboto', sans-serif;background:url(https://i.imgur.com/I5d1ltq.png) no-repeat center center;" align="center">
				<tr>
					<td style="padding: 80px;">
						<table style="border-radius: 4px;" bgcolor="#fff">
							<tr>
								<td>
									<div align="center">
										<img src="https://i.imgur.com/aLmfY9m.png" alt="" style="margin-top: 18px; " >
										<hr style="width: 90%">
									</div>
								</td>
							</tr>
							<tr>
								<td style="padding: 15px;" align="center">
									<span style="color: red; font-size: 25px; font-weight: bold; text-align: center; margin-top: 7px;" >VERIFICATION FAILED</span>

								</td>
							</tr>
							<tr>
								<td style="padding: 15px;" align="center">
									<span style="font-size: 20px; font-weight: 600; color: black; margin-top: 7px;">Either this link has been expired or this link is invalid</span>
									<br>
								</td>
							</tr>
							<tr>
								<td style="padding: 15px;" align="center">
									<span style="font-size: 13px; font-weight: 600; color: #8a8a8a;">If you have any questions, please feel free to contact us at support@qalame.com or by phone at</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
		</table>
	</body>
</html>