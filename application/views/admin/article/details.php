<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Article | Details</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url()?>template/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo base_url()?>template/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
    <!-- BEGIN HEADER -->
    <?php $this->load->view("admin/new_header"); ?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view("admin/new_sidebar"); ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
          <!-- BEGIN PAGE HEAD-->
          <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
              <h1>Article Details</h1>
            </div>
           
          </div>
          
          <ul class="page-breadcrumb breadcrumb">
            <li> <a href="<?php echo base_url()?>dashboard">Home</a> <i class="fa fa-circle"></i> </li>
            <li> <a href="<?php echo base_url()?>Knowledge_center/show_article">Article List</a> <i class="fa fa-circle"></i> </li>
           <!--  <li> <span class="active">Article Details</span> </li> -->
          </ul>
         
          <div class="row">
            <div class="col-md-12">
              <?php if($this->session->flashdata('error')){?>
              <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span> <?php echo $this->session->flashdata('error');?></span> </div>
              <?php }?>
              <?php if($this->session->flashdata('success')){?>
              <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <span> <?php echo $this->session->flashdata('success');?></span> </div>
              <?php }?>
            </div>
            <div class="col-md-6">
              <div class="portlet box yellow">
                <div class="portlet-title">
                  <div class="caption"><!--  <i class="fa fa-book"></i> -->Details</div>
                </div>
                <div class="portlet-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                      <tbody>
                        <tr>
                          <td width="30%"> Title </td>
                          <td><button type="button" class="btn green"> <?php echo $article_data->Title;?> </button></td>
                        </tr>
                        <tr>
                          <td width="30%"> Description </td>
                                                                  
                          <td>  <?php echo $article_data->Description;;?> </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              
            
            <div class="portlet grey-cascade box">
              <div class="portlet-title">
                <div class="caption"> <!-- <i class="fa fa-map-marker"></i> --> Image</div>
                
              </div>
              
              <div class="portlet-body">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered table-striped">
                    <tbody>
                        <tr>
                          <td colspan="2" style="text-align:center"><i class="fa fa-photo ico"></i> Image 
                            <?php if($article_data->Image!=''){?>
                            <br><img src="<?php echo base_url('uploads/article/'.$article_data->Image);?>" width="100%">
                            <?php } else echo "<strong> - No image found!</strong>";?>
                            </td>
                        </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
      </div>
      <!-- END CONTENT BODY -->
    </div>
   
    <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a>
   
  
    <?php $this->load->view("admin/footer"); ?>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>template/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>template/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>template/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script src="<?php echo base_url()?>template/assets/global/plugins/gmaps/gmaps.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="<?php echo base_url()?>template/assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>template/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo base_url()?>template/assets/pages/scripts/profile.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>template/assets/pages/scripts/timeline.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    </body>
<script>
//add Stop start
function search_stop(data)
{
  if(data!="")
  {
	   str = 'data='+data
    //alert(data);
    $.ajax({
		type:"POST",
        url:"<?php echo base_url('stops/stop_name_ajax');?>",
        data:str,
        //url:'ajax.php?status=7&data='+data,
        success:function(data){
            //alert(data);
            $("#search_list_stop").show('fast');
            $('#search_list_stop').empty();
            $('#search_list_stop').append(data);
		}
    });
  }else
  {
    $('#search_list_stop').empty();
  }
}
function add_user_data(id,stop_title)
{
	//alert(id);alert(stop_title);
	$('#stop_title').val(stop_title);
	$('#stop_id_ajax').val(id);
	    
   $("#search_list_stop").hide('fast');
}
//add Stop end

function ValidateNum(input,event)
{
		var keyCode = event.which ? event.which : event.keyCode;
		if((parseInt(keyCode)>=48 && parseInt(keyCode)<=57) || parseInt(keyCode)==46 || parseInt(keyCode)==8 || parseInt(keyCode)==9 || (parseInt(keyCode)>= 37 && parseInt(keyCode) <= 40))	
		{
			return true;
		}
return false;
}
function ValidateText(input,event)
{
		var keyCode = event.which ? event.which : event.keyCode;
		if((parseInt(keyCode)>=47 && parseInt(keyCode)<=64) || (parseInt(keyCode)>= 33 && parseInt(keyCode) <= 45) || (parseInt(keyCode)>= 91 && parseInt(keyCode) <= 95) || (parseInt(keyCode)>= 123 && parseInt(keyCode) <= 125))
		{
			return false;
		}
return true;
}
</script>
</html>