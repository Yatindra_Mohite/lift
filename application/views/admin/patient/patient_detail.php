<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title> Patient|Details</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
       
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
      
        <link href="<?php echo base_url()?>template/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
      
        <link href="<?php echo base_url()?>template/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
       
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
      
        <link rel="shortcut icon" href="favicon.ico" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">

        </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
           <?php $this->load->view("admin/new_header"); ?>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
             <?php $this->load->view("admin/new_sidebar"); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Patient Details
                           
                            </h1>
                        </div>
                       
                        <div class="page-toolbar">
                            
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                   
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url()?>dashboard">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>

                       <!--  <li>
                            <span class="active">Patient List</span>
                        </li> -->
                    </ul>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                            <!-- END EXAMPLE TABLE PORTLET-->
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <!-- <i class="fa fa-globe"></i> -->Patient List </div>
                                    <div class="actions">
                                        
                                    
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                           <tr>
                                                <th> S.No. </th>
                                                <th> Name </th>
                                                <th> Email</th>
                                                <th> DOB</th>
                                                <th> Profile Pic</th>
                                                <th> Gender</th>
                                                <th>Address</th>
                                                <th>Emergency Contact Person</th>
                                                <th>Emergency Contact No.</th>
                                                <th>View Report</th>
                                                <th>View Detail</th>
                                               
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th> S.No. </th>
                                                <th> Name </th>
                                                <th> Email</th>
                                                <th> DOB</th>
                                                <th> Profile Pic</th>
                                                <th> Gender</th>
                                                <th>Address</th>
                                                <th>Emergency Contact Person</th>
                                                <th>Emergency Contact No.</th>
                                                <th>View Report</th>
                                                <th>View Detail</th>
                                                
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        $i=1;
                                        if(!empty($patient_data))
                                        {
                                            foreach($patient_data as $key)
                                            {?>
                                                <tr>
                                                    <td><?php echo $i;?> 
                                                    <td>
                                                        <?php echo $key->Patient_Name;?>
                                                        <p class="font-yellow-gold"><i class='fa fa-mobile-phone'></i> <?php echo $key->Phone_No;?></p>
                                                    </td>
                                                    <td> <?php echo $key->Email;?> </td>
                                                    <td><?php $date = $key->Dob; 
                                                        if(!empty($date))
                                                        {echo $newDate = date("d-m-Y", strtotime($date));}
                                                        else{ ''; }
                                                        

                                                    ?></td>
                                                    <td>
                                                    <?php

                                                     if($key->Profile_pic) 
                                                     {
                                                       $image = $key->Profile_pic;
                                                     }
                                                     else
                                                     {
                                                        $image = 'image-not-found.gif';
                                                     }
                                                        
                                                    ?>    
                                                        
                                                    <img src="<?php echo base_url('uploads/patient_image/'.$image)?>" height="80px" width="90px"></td>
                                                    <td><?php echo $key->Gender;?></td>
                                                    <td><?php echo $key->Address;?></td>
                                                    <td><?php echo $key->Emergency_contact_person;?></td>
                                                    <td><?php echo $key->Emergency_contact;?></td>
                                                    <td><a href="<?php echo base_url('doctor/patient_report/'.$key->Patient_ID) ?>" title="click here to details"><span class="label label-sm label-primary">View Report</i></span></a></td>
                                                    <td><a href="<?php echo base_url('doctor/detail/'.$key->Patient_ID) ?>" title="click here to details"><span class="label label-sm label-primary"><i class="fa fa-info-circle"></i></span></a></td>
                                              
                                                </tr>
                                                  <?php  $i++;
                                             } }
                                          else
                                          {?>
                                        <tr>
                                         <tr class="even pointer">
                                               <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ><?php echo "Record not found";?></td>
                                                <td class=""></td>
                                                <td class=""></td>
                                                <td class=""></td> 
                                            
                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <div id="myModal_add_new_services" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="portlet-title">
                   <div class="caption">
                      <i class=" icon-layers font-red"></i>
                      <span class="caption-subject font-red bold uppercase"> Update Subscription </span>
                   </div>
                 </div>
              </div>
              <div class="modal-body">
               <form class="form-horizontal form-bordered" action="<?php echo base_url('user/update_subscription')?>" method="POST">
                  <div class="form-body">
                  <input type="hidden" id="user11" name="freelance_id" value="">
                  
                    <div class="form-group">
                      <label class="control-label col-md-3">Date</label>
                      <div class="col-md-6">
                        <input type="text" name="subscription_date" id="datepicker" class="form-control jay" placeholder="Date"  required>
                      </div>
                    </div>
                    <div class="">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9"> <br>
                          <button name="submit" id="s" type="submit" class="btn green uppercase" value="Add Services">Submit</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>

              </div>
              <div class="modal-footer"></div>
            </div>
          </div>
        </div>


            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
      <?php $this->load->view("admin/footer"); ?>
        <!-- END FOOTER -->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url()?>template/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/pages/scripts/table-datatables-fixedheader.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>
<script type="text/javascript">
        function changestatus(id,status)
        { 
            var str = "user_id="+id+"&status="+status;
            //alert(str);
            var r = confirm('Are you really want to change status?');
            if(r==true)
            {
                $.ajax({
                  type:"POST",
                   url:"<?php echo base_url('user/change_status')?>/",
                   data:str,
                   success:function(data)
                   {   //alert(data);
                      if(data==1000)
                       {
                            location.reload();
                       }
                   }
                });
            }
        }
    </script>
     <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>

<script type="text/javascript">
function trim(value) {
  value = value.replace(/^\s+/,'');
  value = value.replace(/\s+$/,'');
  return value;
}
</script>
<script>
function getvalue(id,value)
{ //alert(value);
    $('#user11').val(id);
    $('.jay').val(value);
}
</script>