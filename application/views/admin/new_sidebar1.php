 <div class="page-sidebar-wrapper">
                    
                    <div class="page-sidebar navbar-collapse collapse">
                       
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                           
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                           <!--  <li class="sidebar-search-wrapper">
                                <form class="sidebar-search  " action="page_general_search_3.html" method="POST">
                                    <a href="javascript:;" class="remove">
                                        <i class="icon-close"></i>
                                    </a>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn submit">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
                                    </div>
                                </form>
                                
                            </li> -->
                            <li class="nav-item start <?php if($this->uri->segment(1)=='dashboard'){ echo 'active';} ?>">
                                <a href="<?php echo base_url('dashboard');?>" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="selected"></span>
                                    <!--<span class="arrow open"></span> -->
                                </a>
                               <!--  <ul class="sub-menu">
                                    <li class="nav-item start active open">
                                        <a href="index.html" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Dashboard 1</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="dashboard_2.html" class="nav-link ">
                                            <i class="icon-bulb"></i>
                                            <span class="title">Dashboard 2</span>
                                            <span class="badge badge-success">1</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="dashboard_3.html" class="nav-link ">
                                            <i class="icon-graph"></i>
                                            <span class="title">Dashboard 3</span>
                                            <span class="badge badge-danger">5</span>
                                        </a>
                                    </li>
                                </ul> -->
                            </li>
                           <!--  <li class="heading">
                                <h3 class="uppercase">Features</h3>
                            </li> -->
                            <li class="nav-item <?php if($this->uri->segment(1)=='user'){ echo 'active';} ?>">
                                 <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-user"></i>
                                    <span class="title">Users</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(2)=='verified'){ echo 'active';} ?>">
                                        <a href="<?php echo base_url('user/verified');?>" class="nav-link">
                                            <span class="title">Verified User's</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(2)=='unverified'){ echo 'active';} ?>">
                                        <a href="<?php echo base_url('user/unverified');?>" class="nav-link">
                                            <span class="title">Unverified User's</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($this->uri->segment(2)=='deactivate'){ echo 'active';} ?>">
                                        <a href="<?php echo base_url('user/deactivate');?>" class="nav-link">
                                            <span class="title">Deactivate User's</span>
                                        </a>
                                    </li>
                                 
                                </ul>
                            </li>
                        
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>