<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Followers|User's</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php $this->load->view("admin/head.php"); ?>
        </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
           <?php $this->load->view("admin/new_header1"); ?>
            <!-- END HEADER INNER -->
        </div>
        <div class="clearfix"> </div>
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Follower's</div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                           <tr>
                                                <th><center>Image</center></th>
                                                <th><center>Name</center></th>
                                                <th><center>Email</center></th>
                                                <th><center>Gender</center></th>
                                                <th><center>Country </center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>Image</center></th>
                                                <th><center>Name</center></th>
                                                <th><center>Email</center></th>
                                                <th><center>Gender</center></th>
                                                <th><center>Country</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($followers))
                                        {
                                            foreach($followers as $key)
                                            { $date = substr($key['create_date'],0,10);
                                              ?>
                                                <tr>
                                                    <td><center><?php if($key['user_image']){ $image = $key['user_image'];}else{ $image  = 'default-medium.png'; };?>
                                                      <img src="<?php echo base_url('uploads/user_image/'.$image); ?>" width="60px" height="60px" class="img-circle">  
                                                    </center> </td>
                                                    <td><center><?php echo $key['user_name'];?><br>
                                                    <span class="label label-sm label-success badge"><?php echo 'Follow Date - '. $date;?></span>     
                                                    </center></td>
                                                    <td><center><?php echo $key['user_email'];?></center></td>
                                                    <td><center><?php if($key['user_gender'] == 1){ echo '<span class="label label-sm label-success badge"><i class="fa fa-male"></i> Male</span> ';}else if($key['user_gender'] == 2) { echo '<span class="label label-sm label-success badge"><i class="fa fa-female"></i> Female</span>';}?></center></td>
                                                    <td><center><?php $location = $this->common_model->common_getRow('location_list',array('location_id'=>$key['user_location']));
                                                    if(!empty($location->name)){ echo '<span class="label label-sm label-success badge"><i class="fa fa-location-arrow"><b> '.$location->name.'</b></i></span> ';} else { echo ''; }?></center></td>
                                                </tr>
                                                <?php  
                                             } }
                                          else
                                          {?>
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ><?php echo "Record not found";?></td>
                                                <td class=""></td>
                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
          
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
      <?php $this->load->view("admin/footer"); ?>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>




     


